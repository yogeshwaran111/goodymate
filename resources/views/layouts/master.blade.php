<!DOCTYPE html>
<html lang="de">
  
<!-- Mirrored from www.billiger.de/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Apr 2019 04:12:17 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head data-puzzle-compoid="htmlhead">
  <title>billiger.de - Deutschlands beliebter Preisvergleich</title>
  <meta charset="utf-8"/>
  <meta name="theme-color" content="#1B6EC2">
  <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0"/>
  <meta content="index,follow,NOODP" name="robots"/><meta content="Immer den besten Preis im Blick: Mehr als 150.000 Testberichte ✓ Über 400.000 Bewertungen ✓ geprüfte Händler ▶ Jetzt vergleichen &amp; sparen." name="description"><meta name="apple-itunes-app" content="app-id=394665092, app-argument=https://www.billiger.de/"><meta name="referrer" content="origin-when-cross-origin" /><link href="{{url('/assets')}}/index.html" rel="canonical"><link rel="preconnect" href="{{url('/assets')}}/http://cdn.billiger.com/">
  <link rel="dns-prefetch" href="{{url('/assets')}}/http://cdn.billiger.com/">
  <link rel="dns-prefetch" href="{{url('/assets')}}/http://script.ioam.de/">
  <link rel="apple-touch-icon" href="{{url('/assets')}}/static/img/favicon_tile.png">
  <link rel="shortcut icon" type="image/x-icon" href="{{url('/assets')}}/static/img/favicon.ico">
  <link rel="icon" type="image/png" href="{{url('/assets')}}/static/img/favicon_tile.png">
  <link rel="icon" href="{{url('/assets')}}/static/img/favicon.png">
  <link rel="search" type="application/opensearchdescription+xml" title="billiger.de" href="{{url('/assets')}}/osd.xml" />
  <link rel="stylesheet" href="{{url('/assets')}}/cdn.billiger.com/bde6/default/static/build/billiger6-production-325624a603.css">
  <script type="text/javascript">
      window.emos3 = {
          stored: [],
          send: function(p){this.stored.push(p);}
      };
  </script>
  <script type="text/javascript" src="{{url('/assets')}}/static/js/emos.js" async="async"></script>
  <script type="text/javascript" src="{{url('/assets')}}/script.ioam.de/iam.js"></script>
</head>
  <body>
    <div class="global-client-vars" data-puzzle-compoid="global_client_vars"></div>
          <div id="container" class="container">
    <div class="user-panel" data-puzzle-compoid="user_panel">
  </div>
    <div class="header-container">
    @include('layouts.header') 
    </div>
          @yield('pageBody')

    <div class="footer-container">
      
    @include('layouts.footer') 
    </div>
    <div id="eu-cookie-banner" data-puzzle-compoid="eu_cookie_banner">
  <span class="message">
    Für einen besseren Service nutzt billiger.de Cookies. <a href="{{url('/assets')}}/http://company.billiger.de/service/datenschutzunterrichtung/portal-user.php" class="more-info-link">Weitere Informationen</a>.
  </span>
  <span class="svg-icon-close gray pointer close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</span>
</div>
  </div>
  <div
  style="display: none"
  data-puzzle-compoid="main_suggest">
</div>
    <div class="global-redirect-form" data-puzzle-compoid="global_redirect_form">
  <form method="POST" action="https://www.billiger.de/redirect" id="redirectForm">
    <input type="hidden" name="t" value="" id="redirectLink">
  </form>
</div>
    <script type="text/javascript">
      var puzzleCompoParams = {"main_footer":{"compoType":"Footer","animated":false,"renderMode":"initial","connectedCompos":["global_client_vars"],"options":{}},"home_claim":{"compoType":"HomeClaim","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}},"eu_cookie_banner":{"compoType":"EuCookieBanner","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}},"popular_items":{"compoType":"PopularItems","animated":false,"renderMode":"initial","connectedCompos":["eu_label_layer"],"options":{"product_id":{"name":"product_id","in_url":false}}},"main_header":{"compoType":"Header","animated":false,"renderMode":"initial","connectedCompos":["main_navigation","main_navigation_mobile","main_suggest","community_login","main_product_compare_basket","global_client_vars"],"options":{"action":{"name":"action","in_url":false}}},"htmlhead":{"compoType":"HTMLHead","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}},"analytic_econda":{"compoType":"AnalyticEconda","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}},"modal_preloader":{"compoType":"ModalPreloader","animated":false,"renderMode":"hidden","connectedCompos":["global_client_vars"],"options":{}},"main_navigation":{"compoType":"MainNavigation","animated":false,"renderMode":"placeholder","connectedCompos":["main_header"],"options":{}},"main_product_compare_basket":{"compoType":"ProductCompareBasket","animated":false,"renderMode":"hidden","connectedCompos":["main_header","community_login","main_navigation","main_suggest","navigation_bar","global_client_vars"],"options":{"compare_ids":{"name":"compare_product_ids","in_url":false}}},"global_redirect_form":{"compoType":"GlobalRedirectForm","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}},"community_login":{"compoType":"CommunityLogin","animated":false,"renderMode":"hidden","connectedCompos":["main_header","global_client_vars"],"options":{}},"eu_label_layer":{"compoType":"EuLabelLayer","animated":false,"renderMode":"hidden","connectedCompos":["modal_preloader"],"options":{"product_id":{"name":"product_id","in_url":false}}},"top_categories":{"compoType":"TopCategories","animated":false,"renderMode":"initial","connectedCompos":["global_client_vars"],"options":{}},"w3b_survey":{"compoType":"WwwUserSurvey","animated":false,"renderMode":"hidden","connectedCompos":[],"options":{}},"assortment_links":{"compoType":"AssortmentLinks","animated":false,"renderMode":"initial","connectedCompos":["global_client_vars"],"options":{}},"scroll_top_button":{"compoType":"ScrollTopButton","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}},"global_client_vars":{"compoType":"GlobalClientVars","animated":false,"renderMode":"initial","connectedCompos":[],"options":{"globalVars":{"currentRouteName":"home","currentCategoryId":null,"breakpoints":{"small":749,"large":1279,"medium":1019,"xsmall":499,"xlarge":1619}}}},"client_performance_stats":{"compoType":"ClientPerformanceStats","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}},"banderole":{"compoType":"Banderole","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}},"home_selected_items":{"compoType":"HomeSelectedItems","animated":false,"renderMode":"initial","connectedCompos":["global_client_vars"],"options":{}},"main_navigation_mobile":{"compoType":"MainNavigationMobile","animated":false,"renderMode":"placeholder","connectedCompos":[],"options":{}},"main_suggest":{"compoType":"Suggest","animated":false,"renderMode":"hidden","connectedCompos":["global_client_vars","main_header"],"options":{"suggest_query":{"name":"suggest","in_url":false},"last_search_query":{"name":"last_search_query","in_url":false}}},"user_panel":{"compoType":"UserPanel","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}},"deals":{"compoType":"Deals","animated":false,"renderMode":"initial","connectedCompos":["eu_label_layer"],"options":{"product_id":{"name":"product_id","in_url":false}}},"assets_helper":{"compoType":"AssetsHelper","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}},"recently_viewed_products":{"compoType":"RecentlyViewedProducts","animated":false,"renderMode":"hidden","connectedCompos":["eu_label_layer"],"options":{"all_items":{"name":"recently_viewed_items","in_url":false},"show_remove_msg":{"name":"recently_show_remove_msg","in_url":false},"show_remove_all":{"name":"recently_show_remove_all","in_url":false},"max_items":{"name":"recently_viewed_max","in_url":false},"product_id":{"name":"product_id","in_url":false}}},"home_footer_text":{"compoType":"HomeFooterText","animated":false,"renderMode":"initial","connectedCompos":[],"options":{}}};
    </script>
    <script src="{{url('/assets')}}/cdn.billiger.com/bde6/default/static/build/billiger6-production-d249e91bd5.js"></script>
            <div
  style="display: none"
  data-puzzle-compoid="modal_preloader">
</div>
    
  <script type="text/javascript" data-puzzle-compoid="client_performance_stats">

</script>
  <div
  style="display: none"
  data-puzzle-compoid="w3b_survey">
</div>
  
<script type="text/javascript" data-puzzle-compoid="analytic_econda">
  //<![CDATA[
    window.emos3.defaults = {
      siteid : 'www.billiger.de',
      content: 'Startseite'
    };

    var emospro = {};
    emospro.pageId  = 'I1tyN3yGS8Uno159ZwCrAs';
    emospro.entrypage = 'Startseite';

          emospro.tecdata = [['home']];
    
    
    
    
          emospro.bsession = [['LOxE9Xr58yjGhpBv4vz-UDz6tFOJNFN9SLnPKxiRZaO9ikcREROx6Dq3u2Im3N4udjEXnIVtHWz']];
    
    
    try {
      updateEmosCgroups(emospro);
    } catch (e) {};

    try {
      updateEmosNlsource(emospro);
    } catch (e) {};

    window.emos3.send(emospro);
  //]]>
</script>
    <div id="fixed-bottom-container">
        <div class="fixed-bottom-right flex-row justify-content-end">
    <div class="scroll-top-button" data-puzzle-compoid="scroll_top_button" data-econda-marker="Events/TopButton">
  <div>
    Nach oben
  </div>
  <i class="css-icon-arrow small white lighter up arrow"></i>
</div>
  </div>
  <div
  style="display: none"
  data-puzzle-compoid="main_product_compare_basket">
</div>
    </div>
  </body>

<!-- Mirrored from www.billiger.de/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Apr 2019 04:20:39 GMT -->
</html>
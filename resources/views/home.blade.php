@extends('layouts.master')
@section('pageBody')
      <div class="main-navigation placeholder flex-row" data-puzzle-compoid="main_navigation">
  <div class="navmenu menu">
    <div class="spinner">
      <div class="bounce1"></div>
      <div class="bounce2"></div>
      <div class="bounce3"></div>
    </div>
  </div>
</div>
    </div>
        <div class="container-content">
      <div class="main-navigation-mobile" data-puzzle-compoid="main_navigation_mobile" style="display:none;">
  <!--[if !IE]><!-->
<div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
  </div>
<!--<![endif]-->
<!--[if IE]>
  <div class="spinner-ie">
    <img src="{{url('/assets')}}//static/img/ellipsis_small.gif" alt="spinner">
  </div>
<![endif]-->
</div>
      <div
  style="display: none"
  data-puzzle-compoid="community_login">
</div>
              <div class="home-claim" data-puzzle-compoid="home_claim">
  <div class="svg-home-claim left"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 490 100" preserveAspectRatio="xMidYMid slice">
  <defs>
    <style>
      .bde-claim-cls-1{fill:none;}
      .bde-claim-cls-2{clip-path:url(#bde-claim-left-clip-path);}
    </style>
    <clipPath id="bde-claim-left-clip-path">
      <rect class="bde-claim-cls-1" width="490" height="100"/>
    </clipPath>
    <linearGradient id="bde-claim-left-gradient-1" x1="5.35" y1="50" x2="420" y2="50" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#1B6EC2"/>
      <stop offset="0.19" stop-color="#3f5e97"/>
      <stop offset="0.45" stop-color="#7d92b9"/>
      <stop offset="0.67" stop-color="#e1e6ef"/>
      <stop offset="1" stop-color="#fff"/>
    </linearGradient>
    <linearGradient id="bde-claim-left-gradient-2" x1="5.35" y1="50" x2="420" y2="50" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#34669a"/>
      <stop offset="0.19" stop-color="#1b69bd"/>
      <stop offset="0.45" stop-color="#1b69bd"/>
      <stop offset="0.67" stop-color="#a5c6e7"/>
      <stop offset="1" stop-color="#fff"/>
    </linearGradient>
  </defs>
  <g class="bde-claim-cls-2">
    <rect class="bde-claim-cls-3" width="490" height="100"/>
    <polygon class="bde-claim-cls-4" points="44 14.5 0 42 44 69.5 88 42 44 14.5"/>
    <polygon class="bde-claim-cls-5" points="132 14.5 88 42 132 69.5 176 42 132 14.5"/>
    <polygon class="bde-claim-cls-6" points="220 14.5 176 42 220 69.5 264 42 220 14.5"/>
    <polygon class="bde-claim-cls-7" points="308 14.5 264 42 308 69.5 352 42 308 14.5"/>
    <polygon class="bde-claim-cls-8" points="396 14.5 352 42 396 69.5 440 42 396 14.5"/>
    <polygon class="bde-claim-cls-9" points="0 0 0 42 44 14.5 44 0 0 0"/>
    <polygon class="bde-claim-cls-10" points="44 0 44 14.5 88 42 88 0 44 0"/>
    <polygon class="bde-claim-cls-11" points="88 0 88 42 132 14.5 132 0 88 0"/>
    <polygon class="bde-claim-cls-12" points="132 0 132 14.5 176 42 176 0 132 0"/>
    <polygon class="bde-claim-cls-13" points="176 0 176 42 220 14.5 220 0 176 0"/>
    <polygon class="bde-claim-cls-14" points="220 0 220 14.5 264 42 264 0 220 0"/>
    <polygon class="bde-claim-cls-15" points="264 0 264 42 308 14.5 308 0 264 0"/>
    <polygon class="bde-claim-cls-16" points="308 0 308 14.5 352 42 352 0 308 0"/>
    <polygon class="bde-claim-cls-17" points="352 0 352 42 396 14.5 396 0 352 0"/>
    <polygon class="bde-claim-cls-8" points="396 0 396 14.5 440 42 440 0 396 0"/>
    <polygon class="bde-claim-cls-8" points="440 42 484 14.5 484 0 440 0 440 42"/>
    <polygon class="bde-claim-cls-18" points="44 100 44 69.5 0 42 0 86 22.4 100 44 100"/>
    <polygon class="bde-claim-cls-19" points="65.6 100 88 86 88 42 44 69.5 44 100 65.6 100"/>
    <polygon class="bde-claim-cls-20" points="110.4 100 132 100 132 69.5 88 42 88 86 110.4 100"/>
    <polygon class="bde-claim-cls-21" points="153.6 100 176 86 176 42 132 69.5 132 100 153.6 100"/>
    <polygon class="bde-claim-cls-22" points="198.4 100 220 100 220 69.5 176 42 176 86 198.4 100"/>
    <polygon class="bde-claim-cls-14" points="241.6 100 264 86 264 42 220 69.5 220 100 241.6 100"/>
    <polygon class="bde-claim-cls-15" points="286.4 100 308 100 308 69.5 264 42 264 86 286.4 100"/>
    <polygon class="bde-claim-cls-16" points="329.6 100 352 86 352 42 308 69.5 308 100 329.6 100"/>
    <polygon class="bde-claim-cls-17" points="374.4 100 396 100 396 69.5 352 42 352 86 374.4 100"/>
    <polygon class="bde-claim-cls-23" points="417.6 100 440 86 440 42 396 69.5 396 100 417.6 100"/>
    <polygon class="bde-claim-cls-8" points="462.4 100 484 100 484 69.5 440 42 440 86 462.4 100"/>
    <polygon class="bde-claim-cls-24" points="88 86 65.6 100 110.4 100 88 86"/>
    <polygon class="bde-claim-cls-25" points="176 86 153.6 100 198.4 100 176 86"/>
    <polygon class="bde-claim-cls-6" points="264 86 241.6 100 286.4 100 264 86"/>
    <polygon class="bde-claim-cls-26" points="352 86 329.6 100 374.4 100 352 86"/>
    <polygon class="bde-claim-cls-27" points="440 86 417.6 100 462.4 100 440 86"/>
    <polygon class="bde-claim-cls-9" points="22.4 100 0 86 0 100 22.4 100"/>
  </g>
</svg></div>
  <div class="claim-title">
    <h1>Deutschlands beliebter Preisvergleich</h1>
    <span>Es geht immer billiger.de</span>
  </div>
  <div class="svg-home-claim right"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 490 100" preserveAspectRatio="xMidYMid slice">
  <defs>
    <style>
      .bde-claim-cls-1{fill:none;}
      .bde-claim-cls-2{clip-path:url(#bde-claim-right-clip-path);}
    </style>
    <clipPath id="bde-claim-right-clip-path">
      <rect class="bde-claim-cls-1" width="490" height="100" transform="translate(490 100) rotate(-180)"/>
    </clipPath>
    <linearGradient id="bde-claim-right-gradient-1" x1="5.35" y1="50" x2="420" y2="50" gradientTransform="matrix(1, 0, 0, -1, 0, 100)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#1B6EC2"/>
      <stop offset="0.19" stop-color="#3f5e97"/>
      <stop offset="0.45" stop-color="#7d92b9"/>
      <stop offset="0.67" stop-color="#e1e6ef"/>
      <stop offset="1" stop-color="#fff"/>
    </linearGradient>
    <linearGradient id="bde-claim-right-gradient-2" x1="5.35" y1="50" x2="420" y2="50" gradientTransform="matrix(1, 0, 0, -1, 0, 100)" gradientUnits="userSpaceOnUse">
      <stop offset="0" stop-color="#34669a"/>
      <stop offset="0.19" stop-color="#1b69bd"/>
      <stop offset="0.45" stop-color="#1b69bd"/>
      <stop offset="0.67" stop-color="#a5c6e7"/>
      <stop offset="1" stop-color="#fff"/>
    </linearGradient>
  </defs>
  <g class="bde-claim-cls-2">
    <rect class="bde-claim-cls-3" width="490" height="100" transform="translate(490 100) rotate(-180)"/>
    <polygon class="bde-claim-cls-4" points="446 14.5 490 42 446 69.5 402 42 446 14.5"/>
    <polygon class="bde-claim-cls-5" points="358 14.5 402 42 358 69.5 314 42 358 14.5"/>
    <polygon class="bde-claim-cls-6" points="270 14.5 314 42 270 69.5 226 42 270 14.5"/>
    <polygon class="bde-claim-cls-7" points="182 14.5 226 42 182 69.5 138 42 182 14.5"/>
    <polygon class="bde-claim-cls-8" points="94 14.5 138 42 94 69.5 50 42 94 14.5"/>
    <polygon class="bde-claim-cls-9" points="490 0 490 42 446 14.5 446 0 490 0"/>
    <polygon class="bde-claim-cls-10" points="446 0 446 14.5 402 42 402 0 446 0"/>
    <polygon class="bde-claim-cls-11" points="402 0 402 42 358 14.5 358 0 402 0"/>
    <polygon class="bde-claim-cls-12" points="358 0 358 14.5 314 42 314 0 358 0"/>
    <polygon class="bde-claim-cls-13" points="314 0 314 42 270 14.5 270 0 314 0"/>
    <polygon class="bde-claim-cls-14" points="270 0 270 14.5 226 42 226 0 270 0"/>
    <polygon class="bde-claim-cls-15" points="226 0 226 42 182 14.5 182 0 226 0"/>
    <polygon class="bde-claim-cls-16" points="182 0 182 14.5 138 42 138 0 182 0"/>
    <polygon class="bde-claim-cls-17" points="138 0 138 42 94 14.5 94 0 138 0"/>
    <polygon class="bde-claim-cls-8" points="94 0 94 14.5 50 42 50 0 94 0"/>
    <polygon class="bde-claim-cls-8" points="50 42 6 14.5 6 0 50 0 50 42"/>
    <polygon class="bde-claim-cls-18" points="446 100 446 69.5 490 42 490 86 467.6 100 446 100"/>
    <polygon class="bde-claim-cls-19" points="424.4 100 402 86 402 42 446 69.5 446 100 424.4 100"/>
    <polygon class="bde-claim-cls-20" points="379.6 100 358 100 358 69.5 402 42 402 86 379.6 100"/>
    <polygon class="bde-claim-cls-21" points="336.4 100 314 86 314 42 358 69.5 358 100 336.4 100"/>
    <polygon class="bde-claim-cls-22" points="291.6 100 270 100 270 69.5 314 42 314 86 291.6 100"/>
    <polygon class="bde-claim-cls-14" points="248.4 100 226 86 226 42 270 69.5 270 100 248.4 100"/>
    <polygon class="bde-claim-cls-15" points="203.6 100 182 100 182 69.5 226 42 226 86 203.6 100"/>
    <polygon class="bde-claim-cls-16" points="160.4 100 138 86 138 42 182 69.5 182 100 160.4 100"/>
    <polygon class="bde-claim-cls-17" points="115.6 100 94 100 94 69.5 138 42 138 86 115.6 100"/>
    <polygon class="bde-claim-cls-23" points="72.4 100 50 86 50 42 94 69.5 94 100 72.4 100"/>
    <polygon class="bde-claim-cls-8" points="27.6 100 6 100 6 69.5 50 42 50 86 27.6 100"/>
    <polygon class="bde-claim-cls-24" points="402 86 424.4 100 379.6 100 402 86"/>
    <polygon class="bde-claim-cls-25" points="314 86 336.4 100 291.6 100 314 86"/>
    <polygon class="bde-claim-cls-6" points="226 86 248.4 100 203.6 100 226 86"/>
    <polygon class="bde-claim-cls-26" points="138 86 160.4 100 115.6 100 138 86"/>
    <polygon class="bde-claim-cls-27" points="50 86 72.4 100 27.6 100 50 86"/>
    <polygon class="bde-claim-cls-9" points="467.6 100 490 86 490 100 467.6 100"/>
  </g>
</svg></div>
</div>
  <div class="deals" data-puzzle-compoid="deals"><div class="component-heading">
      Unsere besten Deals – jetzt Schnäppchen sichern!<a href="{{url('/assets')}}/deals.html" class="appendage econda-marker" data-econda-marker="Events/Home_v6/ShowMoreDeals">Alle Deals anzeigen</a></div>
    <div class="content-row">
      <div class="swipe-arrow left">
        <i class="css-icon-arrow large gray bold left"></i>
      </div>
      <div class="scroll-tiles scrollbar-horizontal-transparent scrollbar-hidden"><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1319529381-huawei-p20-dual-sim-128gb-schwarz.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Handys ohne Vertrag"  class="headline econda-marker" title="Huawei P20 Dual SIM 128GB schwarz">  <div class="product-image"><span class="deals-value-savings left">- 15%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/olpRRtaycPA395g9X9HAp7mnDeOD-N_Ysmcqo-5Mi-SckuR/huawei-p20-dual-sim-128gb-schwarz.jpg"
        width="200"
        height="200"
        alt="Huawei P20 Dual SIM 128GB schwarz"
      ></div>

              <span class="name">Huawei P20 Dual SIM 128GB schwarz</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>315,99
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
<div class="best-deal">TOP PREIS</div></a></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1229557818-apple-iphone-8-256gb-space-grau.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Handys ohne Vertrag"  class="headline econda-marker" title="Apple iPhone 8 256GB Space Grau">  <div class="product-image"><span class="deals-value-savings left">- 7%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/NtYso51pJZYCsyHTu5Up1TmnDeOD-N_Ysmcqo-5Mi-SckuR/apple-iphone-8-256gb-space-grau.jpg"
        width="200"
        height="200"
        alt="Apple iPhone 8 256GB Space Grau"
      ></div>

              <span class="name">Apple iPhone 8 256GB Space Grau</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>709,30
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</a></div><div class="scroll-item tile"><div data-submit="X0pzyVppXQxQjWVZvyGbK0ErSC2eMeSPPB4Hfg755YXRx50RztRVIxJunkT3G-73qEg4dlkrkCqxDkTFyFtuROSyDmPZy1BrCX2Wnkk_OPzexchtNlCvjY"    data-econda-marker="Events/Home_v6/DealsItemClick/E-Bikes"  class="link-helper headline" title="Prophete Entdecker e8.7 28 Zoll RH 50 cm Damen grau">  <div class="product-image"><span class="deals-value-savings left">- 16%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/RNnnh_qFZPI6GlNuc_MXDrmnDeOD-N_Ysmcqo-5Mi-SckuR/prophete-entdecker-e8-7-28-zoll-rh-50-cm-damen-grau.jpg"
        width="200"
        height="200"
        alt="Prophete Entdecker e8.7 28 Zoll RH 50 cm Damen grau"
      ></div>

              <span class="name">Prophete Entdecker e8.7 28 Zoll RH 50 cm Damen grau</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>1.059,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</div></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1228788592-apple-iphone-8-256gb-silber.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Handys ohne Vertrag"  class="headline econda-marker" title="Apple iPhone 8 256GB Silber">  <div class="product-image"><span class="deals-value-savings left">- 8%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/hmMOalCM3L5oTY4NiKCzhfmnDeOD-N_Ysmcqo-5Mi-SckuR/apple-iphone-8-256gb-silber.jpg"
        width="200"
        height="200"
        alt="Apple iPhone 8 256GB Silber"
      ></div>

              <span class="name">Apple iPhone 8 256GB Silber</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>709,30
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</a></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1380435389-lg-oled65b8lla.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Fernseher"  class="headline econda-marker" title="LG OLED65B8LLA">  <div class="product-image"><span class="deals-value-savings left">- 7%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/KKXZhzHmymx7xuZn9Pzc3DmnDeOD-N_Ysmcqo-5Mi-SckuR/lg-oled65b8lla.jpg"
        width="200"
        height="200"
        alt="LG OLED65B8LLA"
      ><div class="eu-labels">  <div class="eu-label eff-3"
          data-product-id="1380435389" >
    <span class="qualifier">A
    </span>
    <span class="triangle"></span>
  </div>
</div></div>

              <span class="name">LG OLED65B8LLA</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>1.849,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</a></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1323707473-jura-s80-piano-black.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Kaffeevollautomaten"  class="headline econda-marker" title="Jura S80 Piano Black">  <div class="product-image"><span class="deals-value-savings left">- 6%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/pAK7_XYciwQki9FO3trK-bmnDeOD-N_Ysmcqo-5Mi-SckuR/jura-s80-piano-black.jpg"
        width="200"
        height="200"
        alt="Jura S80 Piano Black"
      ></div>

              <span class="name">Jura S80 Piano Black</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>1.099,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</a></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1387484376-huawei-matebook-x-pro-53010dma.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Notebooks"  class="headline econda-marker" title="Huawei MateBook X Pro (53010DMA)">  <div class="product-image"><span class="deals-value-savings left">- 6%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/3qzeJBLfQ9qoPcKVtmVj9bmnDeOD-N_Ysmcqo-5Mi-SckuR/huawei-matebook-x-pro-53010dma.jpg"
        width="200"
        height="200"
        alt="Huawei MateBook X Pro (53010DMA)"
      ></div>

              <span class="name">Huawei MateBook X Pro (53010DMA)</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>1.499,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</a></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/890311089-jura-a1-piano-black.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Kaffeevollautomaten"  class="headline econda-marker" title="Jura A1 Piano Black">  <div class="product-image"><span class="deals-value-savings left">- 9%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/e_ZrI4MgsO-JobFXIMgC7jmnDeOD-N_Ysmcqo-5Mi-SckuR/jura-a1-piano-black.jpg"
        width="200"
        height="200"
        alt="Jura A1 Piano Black"
      ></div>

              <span class="name">Jura A1 Piano Black</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>444,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</a></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1453743416-medion-akoya-s6445-30025324.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Notebooks"  class="headline econda-marker" title="Medion Akoya S6445 (30025324)">  <div class="product-image"><span class="deals-value-savings left">- 9%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/jRsYrg5o7iwZ2iL8EY9pBTmnDeOD-N_Ysmcqo-5Mi-SckuR/medion-akoya-s6445-30025324.jpg"
        width="200"
        height="200"
        alt="Medion Akoya S6445 (30025324)"
      ></div>

              <span class="name">Medion Akoya S6445 (30025324)</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>629,95
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
<div class="best-deal">TOP PREIS</div></a></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1434278426-lenovo-yoga-530-14ikb-81ek00r1ge.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Notebooks"  class="headline econda-marker" title="Lenovo Yoga 530-14IKB (81EK00R1GE)">  <div class="product-image"><span class="deals-value-savings left">- 17%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/aO-oCxQTa9SV73OMAAX2TLmnDeOD-N_Ysmcqo-5Mi-SckuR/lenovo-yoga-530-14ikb-81ek00r1ge.jpg"
        width="200"
        height="200"
        alt="Lenovo Yoga 530-14IKB (81EK00R1GE)"
      ></div>

              <span class="name">Lenovo Yoga 530-14IKB (81EK00R1GE)</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>549,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
<div class="best-deal">TOP PREIS</div></a></div><div class="scroll-item tile"><div data-submit="bC7PIkWXZ5XvqXhv_-dNazWUI_welPOHqy8Ab5hZiZXfTER2iNRqIRFlFI1_EZoRl8F9CSMuFGjVqw7snqU1_FpwDI6j5iarw"    data-econda-marker="Events/Home_v6/DealsItemClick/Handys ohne Vertrag"  class="link-helper headline" title="Huawei Mate 10 Pro Dual SIM 128GB blau">  <div class="product-image"><span class="deals-value-savings left">- 19%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/OtlmON46NKnmHiMIxiGjxrmnDeOD-N_Ysmcqo-5Mi-SckuR/huawei-mate-10-pro-dual-sim-128gb-blau.jpg"
        width="200"
        height="200"
        alt="Huawei Mate 10 Pro Dual SIM 128GB blau"
      ></div>

              <span class="name">Huawei Mate 10 Pro Dual SIM 128GB blau</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>369,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</div></div><div class="scroll-item tile"><div data-submit="tEeNtGcEw154ZZN2ADx7rpAxmjHqdJWZF5Ji3VVvIJsxehJJn-PfcExc6abHwKpg5-vYONGX3Zp"    data-econda-marker="Events/Home_v6/DealsItemClick/Küchenmaschinen"  class="link-helper headline" title="Kenwood kMix KMX750WH">  <div class="product-image"><span class="deals-value-savings left">- 13%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/qwTim8-DVEHI_-dm3_upzHmnDeOD-N_Ysmcqo-5Mi-SckuR/kenwood-kmix-kmx750wh.jpg"
        width="200"
        height="200"
        alt="Kenwood kMix KMX750WH"
      ></div>

              <span class="name">Kenwood kMix KMX750WH</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>199,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</div></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1096137560-nintendo-new-nintendo-2ds-xl-schwarz-tuerkis.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Konsolen"  class="headline econda-marker" title="Nintendo New Nintendo 2DS XL schwarz / türkis">  <div class="product-image"><span class="deals-value-savings left">- 10%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/BPKtof6wmEKGrFXQXsYHJLmnDeOD-N_Ysmcqo-5Mi-SckuR/nintendo-new-nintendo-2ds-xl-schwarz-tuerkis.png"
        width="200"
        height="200"
        alt="Nintendo New Nintendo 2DS XL schwarz / türkis"
      ></div>

              <span class="name">Nintendo New Nintendo 2DS XL schwarz / türkis</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>124,95
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</a></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1306546058-microsoft-surface-pro-12-3-i5-8gb-ram-256gb-ssd-wi-fi.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Tablet PCs"  class="headline econda-marker" title="Microsoft Surface Pro 12.3 i5 8GB RAM 256GB SSD Wi-Fi">  <div class="product-image"><span class="deals-value-savings left">- 6%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/WWN1p3sFZQrc5kTLC7EFELmnDeOD-N_Ysmcqo-5Mi-SckuR/microsoft-surface-pro-12-3-i5-8gb-ram-256gb-ssd-wi-fi.jpg"
        width="200"
        height="200"
        alt="Microsoft Surface Pro 12.3 i5 8GB RAM 256GB SSD Wi-Fi"
      ></div>

              <span class="name">Microsoft Surface Pro 12.3 i5 8GB RAM 256GB SSD Wi-Fi</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>929,21
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</a></div><div class="scroll-item tile"><div data-submit="067-s36vRWvvqXhv_-dNazWUI_welPOHjmH0b1QcDA1OLX4MYm1K2_LWW3Bgb1cU4PdO_cauQf-tvg3bPAZBxdil9Ifl89PkQ"    data-econda-marker="Events/Home_v6/DealsItemClick/Rasenmäher"  class="link-helper headline" title="Einhell GE-CM 18/30 Li inkl. 1 x 3,0 Ah">  <div class="product-image"><span class="deals-value-savings left">- 19%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/JdtKNCKFGpz5XDHhgQHg7nmnDeOD-N_Ysmcqo-5Mi-SckuR/einhell-ge-cm-18-30-li-inkl-1-x-3-0-ah.jpg"
        width="200"
        height="200"
        alt="Einhell GE-CM 18/30 Li inkl. 1 x 3,0 Ah"
      ></div>

              <span class="name">Einhell GE-CM 18/30 Li inkl. 1 x 3,0 Ah</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>99,99
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
<div class="best-deal">TOP PREIS</div></div></div><div class="scroll-item tile"><div data-submit="TTK_6bSmPu21q0crU-tViLZdR2WSu4DG5wX5Qbgb1o5cm0puF52-PPmayg6MvLssGwBHfuXRD-hiv7N_lskQf2QwC8Q_CmPiA"    data-econda-marker="Events/Home_v6/DealsItemClick/E-Bikes"  class="link-helper headline" title="Llobe Blanche Deux 28 Zoll RH 50 cm weiß">  <div class="product-image"><span class="deals-value-savings left">- 9%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/49BNbSsMsrliOA3Jo0PuYbmnDeOD-N_Ysmcqo-5Mi-SckuR/llobe-blanche-deux-28-zoll-rh-50-cm-weiss.jpg"
        width="200"
        height="200"
        alt="Llobe Blanche Deux 28 Zoll RH 50 cm weiß"
      ></div>

              <span class="name">Llobe Blanche Deux 28 Zoll RH 50 cm weiß</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>722,49
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</div></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/1359646217-dell-xps-9570-15-6-i7-2-2ghz-16gb-ram-512gb-ssd-cpc1j.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Notebooks"  class="headline econda-marker" title="Dell XPS 9570 15,6&#34; i7 2,2GHz 16GB RAM 512GB SSD (CPC1J)">  <div class="product-image"><span class="deals-value-savings left">- 5%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/Z4rwXqglmWlpdEF2ZVpHLrmnDeOD-N_Ysmcqo-5Mi-SckuR/dell-xps-9570-15-6-i7-2-2ghz-16gb-ram-512gb-ssd-cpc1j.jpg"
        width="200"
        height="200"
        alt="Dell XPS 9570 15,6&#34; i7 2,2GHz 16GB RAM 512GB SSD (CPC1J)"
      ></div>

              <span class="name">Dell XPS 9570 15,6&#34; i7 2,2GHz 16GB RAM 512GB SSD (CPC1J)</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>1.607,35
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</a></div><div class="scroll-item tile"><div data-submit="TZDjKx5z7Uk4ZZN2ADx7rpAxmjHqdJWZJzJriEfT6HPQ_PtJTL_fAUxc6abHwKpg203rBg7208x"    data-econda-marker="Events/Home_v6/DealsItemClick/Küchenmaschinen"  class="link-helper headline" title="Kenwood kMix KMX750RD">  <div class="product-image"><span class="deals-value-savings left">- 16%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/xkwuhrLySvAziMJo0tVieHmnDeOD-N_Ysmcqo-5Mi-SckuR/kenwood-kmix-kmx750rd.jpg"
        width="200"
        height="200"
        alt="Kenwood kMix KMX750RD"
      ></div>

              <span class="name">Kenwood kMix KMX750RD</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>199,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
<div class="best-deal">TOP PREIS</div></div></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/995402760-liebherr-cbnef-5715-comfort-biofresh-nofrost.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Kühl-Gefrierkombinationen"  class="headline econda-marker" title="Liebherr CBNef 5715 Comfort BioFresh NoFrost">  <div class="product-image"><span class="deals-value-savings left">- 5%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/fh7-TKTMRUBDzKoFnsAa6HmnDeOD-N_Ysmcqo-5Mi-SckuR/liebherr-cbnef-5715-comfort-biofresh-nofrost.jpg"
        width="200"
        height="200"
        alt="Liebherr CBNef 5715 Comfort BioFresh NoFrost"
      ><div class="eu-labels">  <div class="eu-label eff-1"
          data-product-id="995402760" >
    <span class="qualifier">A+++
    </span>
    <span class="triangle"></span>
  </div>
</div></div>

              <span class="name">Liebherr CBNef 5715 Comfort BioFresh NoFrost</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>1.194,68
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
<div class="best-deal">TOP PREIS</div></a></div><div class="scroll-item tile"><a href="{{url('/assets')}}/products/971808503-bosch-gsr-18-2-li-plus-professional-inkl-2-x-4-0-ah-l-boxx-0615990hh5.html"    data-econda-marker="Events/Home_v6/DealsItemClick/Akkuschrauber"  class="headline econda-marker" title="Bosch GSR 18-2-LI Plus Professional inkl. 2 x 4,0 Ah + L-Boxx (0615990HH5)">  <div class="product-image"><span class="deals-value-savings left">- 30%</span><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/0_7Y0edUeV4AA3XxxPnuw3mnDeOD-N_Ysmcqo-5Mi-SckuR/bosch-gsr-18-2-li-plus-professional-inkl-2-x-4-0-ah-l-boxx-0615990hh5.jpg"
        width="200"
        height="200"
        alt="Bosch GSR 18-2-LI Plus Professional inkl. 2 x 4,0 Ah + L-Boxx (0615990HH5)"
      ></div>

              <span class="name">Bosch GSR 18-2-LI Plus Professional inkl. 2 x 4,0 Ah + L-Boxx (0615990HH5)</span>
                                                    <span class="price"><span class="price-show-prefix">ab</span>179,95
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      
</a></div></div>
      <div class="swipe-arrow right">
        <i class="css-icon-arrow large gray bold right"></i>
      </div>
    </div><div class="hint-title">Prozentuale Ersparnis im Vergleich zum durchschnittlichen Bestpreis der Vorwoche.</div></div>
  <div class="top-categories" data-puzzle-compoid="top_categories"><div class="component-heading">Aktuelle Top Kategorien</div>
    <div class="content-row">
      <div class="swipe-arrow left">
        <i class="css-icon-arrow large gray bold left"></i>
      </div>
      <div class="scroll-tiles scrollbar-horizontal-transparent scrollbar-hidden"><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/4373.html" title="Handys ohne Vertrag" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_1/4373-Handys ohne Vertrag">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/5gd0f-LNuwcCqpyXfb4FqzmnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-4373.png" alt="Handys ohne Vertrag" class="image" width="220" height="220">
              <span class="name">Handys ohne Vertrag</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/3653.html" title="Damendüfte" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_2/3653-Damendüfte">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/ixDwTOEWvHRvwOmDqmocinmnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-3653.png" alt="Damendüfte" class="image" width="220" height="220">
              <span class="name">Damendüfte</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/2060.html" title="Fernseher" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_3/2060-Fernseher">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/TzbWBuKpiEE6GcHK8Q53F7mnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-2060.png" alt="Fernseher" class="image" width="220" height="220">
              <span class="name">Fernseher</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/3598.html" title="Staubsauger" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_4/3598-Staubsauger">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/cJsKgzBPf6xKLPwdOs164rmnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-3598.png" alt="Staubsauger" class="image" width="220" height="220">
              <span class="name">Staubsauger</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/106089.html" title="Sommerreifen" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_5/106089-Sommerreifen">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/2YeZmeZ0Thy4QHxBYCfP-nmnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-106089.png" alt="Sommerreifen" class="image" width="220" height="220">
              <span class="name">Sommerreifen</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/8842.html" title="Tablet PCs" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_6/8842-Tablet PCs">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/wf2T526wD5GlTyk5Yb2avbmnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-8842.png" alt="Tablet PCs" class="image" width="220" height="220">
              <span class="name">Tablet PCs</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/103525.html" title="Kühl-Gefrierkombinationen" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_7/103525-Kühl-Gefrierkombinationen">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/OtPhSZDiAlgl1mj26RE6trmnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-103525.png" alt="Kühl-Gefrierkombinationen" class="image" width="220" height="220">
              <span class="name">Kühl-Gefrierkombinationen</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/3551.html" title="Kaffeevollautomaten" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_8/3551-Kaffeevollautomaten">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/GDA-DZM-Zzl-2HpCNFBygfmnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-3551.png" alt="Kaffeevollautomaten" class="image" width="220" height="220">
              <span class="name">Kaffeevollautomaten</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/3596.html" title="Geschirrspüler" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_9/3596-Geschirrspüler">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/dx30VeCK1FwSdgiaDTqP7vmnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-3596.png" alt="Geschirrspüler" class="image" width="220" height="220">
              <span class="name">Geschirrspüler</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/11312.html" title="Lego" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_10/11312-Lego">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/zVi5deiu6FdWFFlnZREcI7mnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-11312.png" alt="Lego" class="image" width="220" height="220">
              <span class="name">Lego</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/14083.html" title="Konsolen" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_11/14083-Konsolen">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/tTOr34t_Fcb2pYMFLEvfozmnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-14083.png" alt="Konsolen" class="image" width="220" height="220">
              <span class="name">Konsolen</span>
            </a>
          </div><div class="scroll-item">
            <a href="{{url('/assets')}}/show/kategorie/4212.html" title="Damenuhren" class="headline econda-marker" data-econda-marker="Events/Home_v6/Top_categories/pos_12/4212-Damenuhren">
              <img src="{{url('/assets')}}/cdn.billiger.com/dynimg/_OMx3NqxR0OKRXWsXFbiLDmnDeOD-N_Ysmcqo-5Mi-SckuR/top-category-4212.png" alt="Damenuhren" class="image" width="220" height="220">
              <span class="name">Damenuhren</span>
            </a>
          </div></div>
      <div class="swipe-arrow right">
        <i class="css-icon-arrow large gray bold right"></i>
      </div>
    </div></div>
  <div class="banderole flex-row hidden-xs hidden-sm hidden-md" data-puzzle-compoid="banderole">
  <div class="lg-4-12 text-center">164.000 Testberichte</div>
  <div class="lg-4-12 text-center">439.000 Produktbewertungen</div>
  <div class="lg-4-12 text-center">22.500 Shops</div>
</div>
  <div
  style="display: none"
  data-puzzle-compoid="recently_viewed_products">
</div>
  <div class="assortment-links" data-puzzle-compoid="assortment_links"><div class="component-heading">Das Sortiment im Überblick</div>
    <div class="component-frame flex-row"><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path class="bde-icon-assortment-cls-1" d="M18.62,5H5.38L2,11.76V17H4a2,2,0,0,0,4,0h8a2,2,0,0,0,4,0h2V11.76Zm-12,2H17.38l2,4H4.62ZM15,15H9V13h6ZM4,13H7v2H4Zm13,2V13h3v2Z"/>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100212.html"
                    title="Auto &amp; Motorrad"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100212-Auto &amp; Motorrad"
                  >
                    Auto &amp; Motorrad
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106089.html"
                        title="Sommerreifen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106089-Sommerreifen"
                      >
                        Sommerreifen
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106091.html"
                        title="Ganzjahresreifen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106091-Ganzjahresreifen"
                      >
                        Ganzjahresreifen
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3102.html"
                        title="PKW-Anhänger"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3102-PKW-Anhänger"
                      >
                        PKW-Anhänger
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11238.html"
                        title="Autoteile"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11238-Autoteile"
                      >
                        Autoteile
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2029.html"
                        title="Navigationssysteme"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2029-Navigationssysteme"
                      >
                        Navigationssysteme
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/4313.html"
                        title="Motorroller"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/4313-Motorroller"
                      >
                        Motorroller
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3119.html"
                        title="Motorradhelme"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3119-Motorradhelme"
                      >
                        Motorradhelme
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106086.html"
                        title="Alufelgen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106086-Alufelgen"
                      >
                        Alufelgen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/113452.html"
                        title="Motoröle"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/113452-Motoröle"
                      >
                        Motoröle
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105007.html"
                        title="Autobatterien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105007-Autobatterien"
                      >
                        Autobatterien
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/108568.html"
                        title="Lederkombis"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/108568-Lederkombis"
                      >
                        Lederkombis
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/108558.html"
                        title="Motorradjacken"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/108558-Motorradjacken"
                      >
                        Motorradjacken
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106083.html"
                        title="Motorradreifen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106083-Motorradreifen"
                      >
                        Motorradreifen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/108729.html"
                        title="Autositzbezüge"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/108729-Autositzbezüge"
                      >
                        Autositzbezüge
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2022.html"
                        title="Autoradios"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2022-Autoradios"
                      >
                        Autoradios
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3126.html"
                        title="Quads"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3126-Quads"
                      >
                        Quads
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3103.html"
                        title="Dachboxen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3103-Dachboxen"
                      >
                        Dachboxen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/112933.html"
                        title="Radzierblenden"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/112933-Radzierblenden"
                      >
                        Radzierblenden
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3116.html"
                        title="Motorradbatterien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3116-Motorradbatterien"
                      >
                        Motorradbatterien
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/111075.html"
                        title="Relingträger"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/111075-Relingträger"
                      >
                        Relingträger
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106085.html"
                        title="Stahlfelgen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106085-Stahlfelgen"
                      >
                        Stahlfelgen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/108732.html"
                        title="Kofferraumwannen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/108732-Kofferraumwannen"
                      >
                        Kofferraumwannen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100212.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100212-Auto &amp; Motorrad"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100212-Auto &amp; Motorrad">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <g>
    <path class="bde-icon-assortment-cls-1" d="M17,5a1,1,0,0,0-1,.71L15.06,9H11V3a1,1,0,0,0-1-1H9A6,6,0,0,0,3,8c0,.32,0,1.71,0,2a1,1,0,0,0,0,.29l2,6A1,1,0,0,0,6,17h8a1,1,0,0,0,1-.71L17.74,7H21V5ZM9,4V7.59L6.4,5A3.94,3.94,0,0,1,9,4ZM5,9V8a4,4,0,0,1,.25-1.34L7.59,9Zm8.26,6H6.72L5.39,11h9.07Z"/>
    <circle class="bde-icon-assortment-cls-1" cx="7" cy="20" r="2"/>
    <circle class="bde-icon-assortment-cls-1" cx="13" cy="20" r="2"/>
  </g>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100127.html"
                    title="Baby &amp; Spielwaren"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100127-Baby &amp; Spielwaren"
                  >
                    Baby &amp; Spielwaren
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103153.html"
                        title="Schulranzen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103153-Schulranzen"
                      >
                        Schulranzen
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11312.html"
                        title="Lego"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11312-Lego"
                      >
                        Lego
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3935.html"
                        title="Kinderwagen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3935-Kinderwagen"
                      >
                        Kinderwagen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3941.html"
                        title="Autokindersitze"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3941-Autokindersitze"
                      >
                        Autokindersitze
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11313.html"
                        title="Playmobil"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11313-Playmobil"
                      >
                        Playmobil
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103152.html"
                        title="Schulrucksäcke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103152-Schulrucksäcke"
                      >
                        Schulrucksäcke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103392.html"
                        title="Sandkästen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103392-Sandkästen"
                      >
                        Sandkästen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/113494.html"
                        title="Bällebad"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/113494-Bällebad"
                      >
                        Bällebad
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3920.html"
                        title="Hochstühle"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3920-Hochstühle"
                      >
                        Hochstühle
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106345.html"
                        title="Schleich"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106345-Schleich"
                      >
                        Schleich
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11307.html"
                        title="Laufräder"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11307-Laufräder"
                      >
                        Laufräder
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11425.html"
                        title="Babyphone"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11425-Babyphone"
                      >
                        Babyphone
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3913.html"
                        title="Babybetten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3913-Babybetten"
                      >
                        Babybetten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3921.html"
                        title="Laufgitter"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3921-Laufgitter"
                      >
                        Laufgitter
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103151.html"
                        title="Schulmäppchen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103151-Schulmäppchen"
                      >
                        Schulmäppchen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/102924.html"
                        title="Stillkissen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/102924-Stillkissen"
                      >
                        Stillkissen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3925.html"
                        title="Wickeltische"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3925-Wickeltische"
                      >
                        Wickeltische
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105863.html"
                        title="Schulhefte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105863-Schulhefte"
                      >
                        Schulhefte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11305.html"
                        title="Bobby Cars"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11305-Bobby Cars"
                      >
                        Bobby Cars
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3916.html"
                        title="Reisebetten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3916-Reisebetten"
                      >
                        Reisebetten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105710.html"
                        title="Planschbecken"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105710-Planschbecken"
                      >
                        Planschbecken
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100127.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100127-Baby &amp; Spielwaren"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100127-Baby &amp; Spielwaren">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <g>
    <path class="bde-icon-assortment-cls-1" d="M21,5H18a2,2,0,0,0-2,2V17a2,2,0,0,0,2,2h3a2,2,0,0,0,2-2V7A2,2,0,0,0,21,5ZM18,17V7h3V17Z"/>
    <path class="bde-icon-assortment-cls-1" d="M13,5H3A2,2,0,0,0,1,7v7a2,2,0,0,0,2,2H7v1H5v2h6V17H9V16h4a2,2,0,0,0,2-2V7A2,2,0,0,0,13,5ZM3,14V7H13v7Z"/>
  </g>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100019.html"
                    title="Computer &amp; Software"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100019-Computer &amp; Software"
                  >
                    Computer &amp; Software
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103234.html"
                        title="Tintenpatronen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103234-Tintenpatronen"
                      >
                        Tintenpatronen
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2303.html"
                        title="Notebooks"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2303-Notebooks"
                      >
                        Notebooks
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/8842.html"
                        title="Tablet PCs"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/8842-Tablet PCs"
                      >
                        Tablet PCs
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2073.html"
                        title="Multifunktionsgeräte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2073-Multifunktionsgeräte"
                      >
                        Multifunktionsgeräte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/9205.html"
                        title="PCI Express Grafikkarten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/9205-PCI Express Grafikkarten"
                      >
                        PCI Express Grafikkarten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2117.html"
                        title="Monitore"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2117-Monitore"
                      >
                        Monitore
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2096.html"
                        title="PCs"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2096-PCs"
                      >
                        PCs
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103235.html"
                        title="Toner"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103235-Toner"
                      >
                        Toner
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103957.html"
                        title="SSD Festplatten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103957-SSD Festplatten"
                      >
                        SSD Festplatten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10527.html"
                        title="Externe Festplatten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10527-Externe Festplatten"
                      >
                        Externe Festplatten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2093.html"
                        title="Tastaturen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2093-Tastaturen"
                      >
                        Tastaturen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11025.html"
                        title="SATA Festplatten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11025-SATA Festplatten"
                      >
                        SATA Festplatten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103212.html"
                        title="E-Book-Reader"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103212-E-Book-Reader"
                      >
                        E-Book-Reader
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/8872.html"
                        title="USB-Sticks"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/8872-USB-Sticks"
                      >
                        USB-Sticks
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2077.html"
                        title="Laserdrucker"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2077-Laserdrucker"
                      >
                        Laserdrucker
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/110506.html"
                        title="PC-Aufrüstkits"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/110506-PC-Aufrüstkits"
                      >
                        PC-Aufrüstkits
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104512.html"
                        title="PC-Headsets"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104512-PC-Headsets"
                      >
                        PC-Headsets
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/111857.html"
                        title="DDR4 RAM"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/111857-DDR4 RAM"
                      >
                        DDR4 RAM
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10525.html"
                        title="PC-Lautsprecher"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10525-PC-Lautsprecher"
                      >
                        PC-Lautsprecher
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/29441.html"
                        title="CD Rohlinge"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/29441-CD Rohlinge"
                      >
                        CD Rohlinge
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/29442.html"
                        title="DVD Rohlinge"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/29442-DVD Rohlinge"
                      >
                        DVD Rohlinge
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2088.html"
                        title="Grafiktabletts"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2088-Grafiktabletts"
                      >
                        Grafiktabletts
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100019.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100019-Computer &amp; Software"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100019-Computer &amp; Software">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <g>
    <path class="bde-icon-assortment-cls-1" d="M12,8a5,5,0,1,0,5,5A5,5,0,0,0,12,8Zm0,8a3,3,0,1,1,3-3A3,3,0,0,1,12,16Z"/>
    <path class="bde-icon-assortment-cls-1" d="M20,6H16.54L15.13,3.89A2,2,0,0,0,13.46,3H10.54a2,2,0,0,0-1.67.89L7.46,6H4A2,2,0,0,0,2,8V19a2,2,0,0,0,2,2H20a2,2,0,0,0,2-2V8A2,2,0,0,0,20,6ZM4,19V8H8.54l2-3h2.92l2,3H20V19Z"/>
  </g>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/106994.html"
                    title="Fotografie"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/106994-Fotografie"
                  >
                    Fotografie
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107821.html"
                        title="Systemkameras"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107821-Systemkameras"
                      >
                        Systemkameras
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104146.html"
                        title="Digitale Kompaktkameras"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104146-Digitale Kompaktkameras"
                      >
                        Digitale Kompaktkameras
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/113997.html"
                        title="Drohnen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/113997-Drohnen"
                      >
                        Drohnen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104145.html"
                        title="Digitale Spiegelreflexkameras"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104145-Digitale Spiegelreflexkameras"
                      >
                        Digitale Spiegelreflexkameras
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2199.html"
                        title="Objektive"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2199-Objektive"
                      >
                        Objektive
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10541.html"
                        title="Einwegkameras"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10541-Einwegkameras"
                      >
                        Einwegkameras
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/8870.html"
                        title="SD-Karten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/8870-SD-Karten"
                      >
                        SD-Karten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/113996.html"
                        title="Action Cams"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/113996-Action Cams"
                      >
                        Action Cams
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/9668.html"
                        title="Ferngläser"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/9668-Ferngläser"
                      >
                        Ferngläser
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2194.html"
                        title="Camcorder"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2194-Camcorder"
                      >
                        Camcorder
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103379.html"
                        title="Fotoalben"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103379-Fotoalben"
                      >
                        Fotoalben
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/6495.html"
                        title="Kamerataschen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/6495-Kamerataschen"
                      >
                        Kamerataschen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11239.html"
                        title="Fotostudio Zubehör"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11239-Fotostudio Zubehör"
                      >
                        Fotostudio Zubehör
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/111002.html"
                        title="SpyCams &amp; DashCams"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/111002-SpyCams &amp; DashCams"
                      >
                        SpyCams &amp; DashCams
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2192.html"
                        title="Sofortbildkameras"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2192-Sofortbildkameras"
                      >
                        Sofortbildkameras
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/113998.html"
                        title="Zubehör für Drohnen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/113998-Zubehör für Drohnen"
                      >
                        Zubehör für Drohnen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10561.html"
                        title="Stative"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10561-Stative"
                      >
                        Stative
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/111596.html"
                        title="Zubehör für ActionCams"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/111596-Zubehör für ActionCams"
                      >
                        Zubehör für ActionCams
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/102974.html"
                        title="Digitale Bilderrahmen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/102974-Digitale Bilderrahmen"
                      >
                        Digitale Bilderrahmen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11574.html"
                        title="Fotobücher und Fotogeschenke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11574-Fotobücher und Fotogeschenke"
                      >
                        Fotobücher und Fotogeschenke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106994.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106994-Fotografie"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/106994-Fotografie">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path class="bde-icon-assortment-cls-1" d="M19.56,3.17a1,1,0,0,0-.93-.1l-10,4A1,1,0,0,0,8,8v6.35A3.45,3.45,0,0,0,6.5,14,3.5,3.5,0,1,0,10,17.5V8.68l8-3.2v5.87A3.45,3.45,0,0,0,16.5,11,3.5,3.5,0,1,0,20,14.5V4A1,1,0,0,0,19.56,3.17ZM6.5,19A1.5,1.5,0,1,1,8,17.5,1.5,1.5,0,0,1,6.5,19Zm10-3A1.5,1.5,0,1,1,18,14.5,1.5,1.5,0,0,1,16.5,16Z"/>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/113433.html"
                    title="Freizeit &amp; Musik"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/113433-Freizeit &amp; Musik"
                  >
                    Freizeit &amp; Musik
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11735.html"
                        title="Basteln"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11735-Basteln"
                      >
                        Basteln
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/114322.html"
                        title="Malen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/114322-Malen"
                      >
                        Malen
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106481.html"
                        title="TV-Fanartikel"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106481-TV-Fanartikel"
                      >
                        TV-Fanartikel
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103994.html"
                        title="E-Gitarren"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103994-E-Gitarren"
                      >
                        E-Gitarren
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11247.html"
                        title="Stricken"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11247-Stricken"
                      >
                        Stricken
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103996.html"
                        title="Akustikgitarren"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103996-Akustikgitarren"
                      >
                        Akustikgitarren
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/14445.html"
                        title="Raucher-Zubehör"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/14445-Raucher-Zubehör"
                      >
                        Raucher-Zubehör
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3864.html"
                        title="Keyboards"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3864-Keyboards"
                      >
                        Keyboards
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106534.html"
                        title="PA-Boxen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106534-PA-Boxen"
                      >
                        PA-Boxen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3875.html"
                        title="Licht &amp; Bühne"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3875-Licht &amp; Bühne"
                      >
                        Licht &amp; Bühne
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103975.html"
                        title="Gitarrenverstärker"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103975-Gitarrenverstärker"
                      >
                        Gitarrenverstärker
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3878.html"
                        title="Münzen &amp; Scheine"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3878-Münzen &amp; Scheine"
                      >
                        Münzen &amp; Scheine
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3863.html"
                        title="Klaviere &amp; Pianos"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3863-Klaviere &amp; Pianos"
                      >
                        Klaviere &amp; Pianos
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106533.html"
                        title="Mischpulte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106533-Mischpulte"
                      >
                        Mischpulte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11279.html"
                        title="DJ-Konsolen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11279-DJ-Konsolen"
                      >
                        DJ-Konsolen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3857.html"
                        title="Blechblasinstrumente"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3857-Blechblasinstrumente"
                      >
                        Blechblasinstrumente
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11246.html"
                        title="Sticken"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11246-Sticken"
                      >
                        Sticken
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3859.html"
                        title="Akkordeons"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3859-Akkordeons"
                      >
                        Akkordeons
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/113433.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/113433-Freizeit &amp; Musik"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/113433-Freizeit &amp; Musik">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <g>
    <path class="bde-icon-assortment-cls-1" d="M20,7H16V4a1,1,0,0,0-1-1H9A1,1,0,0,0,8,4V7H4A2,2,0,0,0,2,9V19a2,2,0,0,0,2,2H20a2,2,0,0,0,2-2V9A2,2,0,0,0,20,7ZM10,5h4V7H10ZM20,19H4V9H20Z"/>
    <polygon class="bde-icon-assortment-cls-1" points="11 17 13 17 13 15 15 15 15 13 13 13 13 11 11 11 11 13 9 13 9 15 11 15 11 17"/>
  </g>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100133.html"
                    title="Gesundheit &amp; Kosmetik"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100133-Gesundheit &amp; Kosmetik"
                  >
                    Gesundheit &amp; Kosmetik
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104400.html"
                        title="Arzneimittel"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104400-Arzneimittel"
                      >
                        Arzneimittel
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3653.html"
                        title="Damendüfte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3653-Damendüfte"
                      >
                        Damendüfte
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3654.html"
                        title="Herrendüfte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3654-Herrendüfte"
                      >
                        Herrendüfte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/9025.html"
                        title="Elektrische Zahnbürsten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/9025-Elektrische Zahnbürsten"
                      >
                        Elektrische Zahnbürsten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/9027.html"
                        title="Elektrische Rasierer"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/9027-Elektrische Rasierer"
                      >
                        Elektrische Rasierer
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/9028.html"
                        title="Haar- &amp; Bartschneider"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/9028-Haar- &amp; Bartschneider"
                      >
                        Haar- &amp; Bartschneider
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11230.html"
                        title="Tageslinsen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11230-Tageslinsen"
                      >
                        Tageslinsen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10635.html"
                        title="Brillen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10635-Brillen"
                      >
                        Brillen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3681.html"
                        title="Haarentfernung"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3681-Haarentfernung"
                      >
                        Haarentfernung
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/31773.html"
                        title="Inkontinenz"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/31773-Inkontinenz"
                      >
                        Inkontinenz
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3614.html"
                        title="Tagespflege"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3614-Tagespflege"
                      >
                        Tagespflege
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103061.html"
                        title="Glätteisen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103061-Glätteisen"
                      >
                        Glätteisen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/9034.html"
                        title="Haartrockner"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/9034-Haartrockner"
                      >
                        Haartrockner
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10646.html"
                        title="Massagebänke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10646-Massagebänke"
                      >
                        Massagebänke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/9074.html"
                        title="Massagegeräte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/9074-Massagegeräte"
                      >
                        Massagegeräte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3622.html"
                        title="Kosmetikspiegel"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3622-Kosmetikspiegel"
                      >
                        Kosmetikspiegel
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107363.html"
                        title="Lockenstäbe"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107363-Lockenstäbe"
                      >
                        Lockenstäbe
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11653.html"
                        title="Diabetes-Teststreifen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11653-Diabetes-Teststreifen"
                      >
                        Diabetes-Teststreifen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10658.html"
                        title="Fieberthermometer"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10658-Fieberthermometer"
                      >
                        Fieberthermometer
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10665.html"
                        title="Wärmepflaster"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10665-Wärmepflaster"
                      >
                        Wärmepflaster
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10657.html"
                        title="Inhaliergeräte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10657-Inhaliergeräte"
                      >
                        Inhaliergeräte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/9641.html"
                        title="Fußbäder"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/9641-Fußbäder"
                      >
                        Fußbäder
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100133.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100133-Gesundheit &amp; Kosmetik"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100133-Gesundheit &amp; Kosmetik">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <g>
    <path class="bde-icon-assortment-cls-1" d="M21,6H9A2,2,0,0,0,8,4.28V1H6V4H3A2,2,0,0,0,1,6V21a2,2,0,0,0,2,2H7a2,2,0,0,0,2-2H21a2,2,0,0,0,2-2V8A2,2,0,0,0,21,6ZM7,21H3V6H7Zm2-2V8H21V19Z"/>
    <rect class="bde-icon-assortment-cls-1" x="11" y="9" width="8" height="2"/>
    <circle class="bde-icon-assortment-cls-1" cx="12" cy="14" r="1"/>
    <circle class="bde-icon-assortment-cls-1" cx="15" cy="14" r="1"/>
    <circle class="bde-icon-assortment-cls-1" cx="18" cy="14" r="1"/>
    <circle class="bde-icon-assortment-cls-1" cx="12" cy="17" r="1"/>
    <circle class="bde-icon-assortment-cls-1" cx="15" cy="17" r="1"/>
    <circle class="bde-icon-assortment-cls-1" cx="18" cy="17" r="1"/>
  </g>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100050.html"
                    title="Handy &amp; Telefon"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100050-Handy &amp; Telefon"
                  >
                    Handy &amp; Telefon
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/4373.html"
                        title="Handys ohne Vertrag"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/4373-Handys ohne Vertrag"
                      >
                        Handys ohne Vertrag
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/110456.html"
                        title="Smartwatches"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/110456-Smartwatches"
                      >
                        Smartwatches
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105803.html"
                        title="Handytaschen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105803-Handytaschen"
                      >
                        Handytaschen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107963.html"
                        title="Schnurlose Telefone"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107963-Schnurlose Telefone"
                      >
                        Schnurlose Telefone
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104183.html"
                        title="Mobilteile"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104183-Mobilteile"
                      >
                        Mobilteile
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/4375.html"
                        title="Handy Akkus"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/4375-Handy Akkus"
                      >
                        Handy Akkus
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/112329.html"
                        title="Handy-Gadgets"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/112329-Handy-Gadgets"
                      >
                        Handy-Gadgets
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/4377.html"
                        title="Handy Ladegeräte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/4377-Handy Ladegeräte"
                      >
                        Handy Ladegeräte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/109938.html"
                        title="Einbauteile"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/109938-Einbauteile"
                      >
                        Einbauteile
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/111595.html"
                        title="Smartwatch Zubehör"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/111595-Smartwatch Zubehör"
                      >
                        Smartwatch Zubehör
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/111003.html"
                        title="PowerBanks"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/111003-PowerBanks"
                      >
                        PowerBanks
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/4317.html"
                        title="Funkgeräte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/4317-Funkgeräte"
                      >
                        Funkgeräte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107964.html"
                        title="Schnurgebundene Telefone"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107964-Schnurgebundene Telefone"
                      >
                        Schnurgebundene Telefone
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/4342.html"
                        title="Handy-Headsets"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/4342-Handy-Headsets"
                      >
                        Handy-Headsets
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/4356.html"
                        title="VoIP Telefone"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/4356-VoIP Telefone"
                      >
                        VoIP Telefone
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100050.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100050-Handy &amp; Telefon"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100050-Handy &amp; Telefon">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <g>
    <path class="bde-icon-assortment-cls-1" d="M21,2H7A4,4,0,0,0,3,6V9a1,1,0,0,0,1,1H6v1.28l-2.32.77A1,1,0,0,0,3,13v3a1,1,0,0,0,.45.83l3,2a1,1,0,0,0,1.1,0l3-2A1,1,0,0,0,11,16V13a1,1,0,0,0-.68-.95L8,11.28V10h4V20H2v2H22V3A1,1,0,0,0,21,2ZM6,16.13l-1-.67V13.72l1-.33Zm3-2.41v1.74l-1,.67V13.39ZM20,16v4H14V9a1,1,0,0,0-1-1H5V6A2,2,0,0,1,7,4h6V7h2V4h1V7h2V4h2Z"/>
    <path class="bde-icon-assortment-cls-1" d="M17,13a3,3,0,1,0,3,3A3,3,0,0,0,17,13Zm0,4a1,1,0,1,1,1-1A1,1,0,0,1,17,17Z"/>
  </g>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100162.html"
                    title="Haushalt"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100162-Haushalt"
                  >
                    Haushalt
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3598.html"
                        title="Staubsauger"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3598-Staubsauger"
                      >
                        Staubsauger
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103525.html"
                        title="Kühl-Gefrierkombinationen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103525-Kühl-Gefrierkombinationen"
                      >
                        Kühl-Gefrierkombinationen
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103645.html"
                        title="Frontlader"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103645-Frontlader"
                      >
                        Frontlader
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3551.html"
                        title="Kaffeevollautomaten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3551-Kaffeevollautomaten"
                      >
                        Kaffeevollautomaten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3596.html"
                        title="Geschirrspüler"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3596-Geschirrspüler"
                      >
                        Geschirrspüler
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106860.html"
                        title="Wassersprudler"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106860-Wassersprudler"
                      >
                        Wassersprudler
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103531.html"
                        title="Side-by-Side-Kühlschränke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103531-Side-by-Side-Kühlschränke"
                      >
                        Side-by-Side-Kühlschränke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/9056.html"
                        title="Küchenmaschinen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/9056-Küchenmaschinen"
                      >
                        Küchenmaschinen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3586.html"
                        title="Gefrierschränke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3586-Gefrierschränke"
                      >
                        Gefrierschränke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3549.html"
                        title="Kaffeemaschinen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3549-Kaffeemaschinen"
                      >
                        Kaffeemaschinen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103931.html"
                        title="Herd-Sets"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103931-Herd-Sets"
                      >
                        Herd-Sets
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104643.html"
                        title="Wärmepumpentrockner"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104643-Wärmepumpentrockner"
                      >
                        Wärmepumpentrockner
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3566.html"
                        title="Mikrowellen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3566-Mikrowellen"
                      >
                        Mikrowellen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103646.html"
                        title="Toplader"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103646-Toplader"
                      >
                        Toplader
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3548.html"
                        title="Espressomaschinen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3548-Espressomaschinen"
                      >
                        Espressomaschinen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103643.html"
                        title="Waschtrockner"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103643-Waschtrockner"
                      >
                        Waschtrockner
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103929.html"
                        title="Standherde"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103929-Standherde"
                      >
                        Standherde
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104641.html"
                        title="Kondenstrockner"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104641-Kondenstrockner"
                      >
                        Kondenstrockner
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103701.html"
                        title="Wäscheständer"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103701-Wäscheständer"
                      >
                        Wäscheständer
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103528.html"
                        title="Minikühlschränke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103528-Minikühlschränke"
                      >
                        Minikühlschränke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3587.html"
                        title="Gefriertruhen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3587-Gefriertruhen"
                      >
                        Gefriertruhen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103031.html"
                        title="Vakuumierer"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103031-Vakuumierer"
                      >
                        Vakuumierer
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100162.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100162-Haushalt"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100162-Haushalt">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path class="bde-icon-assortment-cls-1" d="M22,9a5,5,0,0,0-5-5H11A2,2,0,0,0,9,6H7A1,1,0,0,0,6,7V8H2v2H6v1a1,1,0,0,0,1,1H9a2,2,0,0,0,2,2h2v2a1,1,0,0,0,1,1h3.1l.57,2H16v2h3a1,1,0,0,0,.8-.4,1,1,0,0,0,.16-.87l-1.69-5.91A5,5,0,0,0,22,9ZM8,10V8H9v2Zm3,2V6h2v6Zm4,3V14h1.25l.28,1Zm2-3H15V6h2a3,3,0,0,1,0,6Z"/>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100199.html"
                    title="Heimwerken &amp; Garten"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100199-Heimwerken &amp; Garten"
                  >
                    Heimwerken &amp; Garten
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/110208.html"
                        title="Loungemöbel"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/110208-Loungemöbel"
                      >
                        Loungemöbel
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3273.html"
                        title="Grills"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3273-Grills"
                      >
                        Grills
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104391.html"
                        title="Sichtschutz"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104391-Sichtschutz"
                      >
                        Sichtschutz
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104865.html"
                        title="Akkuschrauber"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104865-Akkuschrauber"
                      >
                        Akkuschrauber
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105337.html"
                        title="Gartenhäuser"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105337-Gartenhäuser"
                      >
                        Gartenhäuser
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3272.html"
                        title="Gartenzäune"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3272-Gartenzäune"
                      >
                        Gartenzäune
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103331.html"
                        title="Pavillons"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103331-Pavillons"
                      >
                        Pavillons
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103332.html"
                        title="Gartenmöbel-Sets"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103332-Gartenmöbel-Sets"
                      >
                        Gartenmöbel-Sets
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103814.html"
                        title="Whirlpools"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103814-Whirlpools"
                      >
                        Whirlpools
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105310.html"
                        title="Laminat-Bodenbeläge"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105310-Laminat-Bodenbeläge"
                      >
                        Laminat-Bodenbeläge
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105772.html"
                        title="Pools"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105772-Pools"
                      >
                        Pools
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11689.html"
                        title="Gewächshäuser"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11689-Gewächshäuser"
                      >
                        Gewächshäuser
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107954.html"
                        title="Waffenschränke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107954-Waffenschränke"
                      >
                        Waffenschränke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103824.html"
                        title="Duschwände"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103824-Duschwände"
                      >
                        Duschwände
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105314.html"
                        title="PVC-Bodenbeläge"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105314-PVC-Bodenbeläge"
                      >
                        PVC-Bodenbeläge
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104753.html"
                        title="Türsprechanlagen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104753-Türsprechanlagen"
                      >
                        Türsprechanlagen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3557.html"
                        title="Klimageräte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3557-Klimageräte"
                      >
                        Klimageräte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103348.html"
                        title="Strandkörbe"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103348-Strandkörbe"
                      >
                        Strandkörbe
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104756.html"
                        title="Heizkörper"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104756-Heizkörper"
                      >
                        Heizkörper
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103327.html"
                        title="Hollywoodschaukeln"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103327-Hollywoodschaukeln"
                      >
                        Hollywoodschaukeln
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/109309.html"
                        title="Carports"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/109309-Carports"
                      >
                        Carports
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103616.html"
                        title="Hauswasserwerke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103616-Hauswasserwerke"
                      >
                        Hauswasserwerke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100199.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100199-Heimwerken &amp; Garten"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100199-Heimwerken &amp; Garten">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <g>
    <path class="bde-icon-assortment-cls-1" d="M20.71,2.29a1,1,0,0,0-1.09-.21A1,1,0,0,0,19,3V22h2V16h2a1,1,0,0,0,1-1C24,5.69,20.84,2.43,20.71,2.29ZM21,14V7.24A25.49,25.49,0,0,1,22,14Z"/>
    <path class="bde-icon-assortment-cls-1" d="M8,6V2H6V6A2,2,0,0,1,5,7.72V2H3V7.72A2,2,0,0,1,2,6V2H0V6A4,4,0,0,0,3,9.86V22H5V9.86A4,4,0,0,0,8,6Z"/>
    <path class="bde-icon-assortment-cls-1" d="M12,9a6,6,0,1,0,6,6A6,6,0,0,0,12,9Zm0,10a4,4,0,1,1,4-4A4,4,0,0,1,12,19Z"/>
    <path class="bde-icon-assortment-cls-1" d="M13.5,13A1.5,1.5,0,0,0,12,14.5a1.5,1.5,0,0,0-3,0C9,16,12,18,12,18s3-2,3-3.5A1.5,1.5,0,0,0,13.5,13Z"/>
  </g>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100220.html"
                    title="Lebensmittel &amp; Getränke"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100220-Lebensmittel &amp; Getränke"
                  >
                    Lebensmittel &amp; Getränke
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107286.html"
                        title="Whisky"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107286-Whisky"
                      >
                        Whisky
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105643.html"
                        title="Kaffeepads"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105643-Kaffeepads"
                      >
                        Kaffeepads
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107341.html"
                        title="Sirup"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107341-Sirup"
                      >
                        Sirup
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3814.html"
                        title="Rotweine"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3814-Rotweine"
                      >
                        Rotweine
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107288.html"
                        title="Rum"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107288-Rum"
                      >
                        Rum
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10624.html"
                        title="Champagner"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10624-Champagner"
                      >
                        Champagner
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107284.html"
                        title="Likör &amp; Kräuterschnaps"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107284-Likör &amp; Kräuterschnaps"
                      >
                        Likör &amp; Kräuterschnaps
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3815.html"
                        title="Weißweine"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3815-Weißweine"
                      >
                        Weißweine
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107289.html"
                        title="Wodka"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107289-Wodka"
                      >
                        Wodka
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3786.html"
                        title="Schokolade"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3786-Schokolade"
                      >
                        Schokolade
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3781.html"
                        title="Nüsse"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3781-Nüsse"
                      >
                        Nüsse
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/109703.html"
                        title="Gin"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/109703-Gin"
                      >
                        Gin
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/109770.html"
                        title="Honig"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/109770-Honig"
                      >
                        Honig
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10622.html"
                        title="Sekt"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10622-Sekt"
                      >
                        Sekt
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3806.html"
                        title="Gemahlener Kaffee"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3806-Gemahlener Kaffee"
                      >
                        Gemahlener Kaffee
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10625.html"
                        title="Bier"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10625-Bier"
                      >
                        Bier
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3820.html"
                        title="Mineralwasser"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3820-Mineralwasser"
                      >
                        Mineralwasser
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3808.html"
                        title="Kaffeebohnen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3808-Kaffeebohnen"
                      >
                        Kaffeebohnen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10628.html"
                        title="Softdrinks"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10628-Softdrinks"
                      >
                        Softdrinks
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107287.html"
                        title="Weinbrände"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107287-Weinbrände"
                      >
                        Weinbrände
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10623.html"
                        title="Prosecco"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10623-Prosecco"
                      >
                        Prosecco
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10614.html"
                        title="Eiscreme"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10614-Eiscreme"
                      >
                        Eiscreme
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100220.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100220-Lebensmittel &amp; Getränke"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100220-Lebensmittel &amp; Getränke">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path class="bde-icon-assortment-cls-1" d="M21.6,7.2l-4-3A1,1,0,0,0,17,4H15.41l-.7-.71A1,1,0,0,0,14,3H10a1.05,1.05,0,0,0-.71.29L8.59,4H7a1,1,0,0,0-.6.2l-4,3a1,1,0,0,0-.29,1.25l2,4a1,1,0,0,0,1.34.44L6,12.62V19a1,1,0,0,0,1,1H17a1,1,0,0,0,1-1V12.62l.55.27a1,1,0,0,0,1.34-.44l2-4A1,1,0,0,0,21.6,7.2Zm-8-2.2L12,6.59,10.41,5Zm5,5.66-1.1-.55A1,1,0,0,0,16,11v7H8V11a1,1,0,0,0-1.45-.89l-1.1.55L4.27,8.3,7.33,6H8.59L11,8.41V12h2V8.41L15.41,6h1.26l3.06,2.3Z"/>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100110.html"
                    title="Mode &amp; Schuhe"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100110-Mode &amp; Schuhe"
                  >
                    Mode &amp; Schuhe
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104515.html"
                        title="Herren-Sneaker"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104515-Herren-Sneaker"
                      >
                        Herren-Sneaker
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2409.html"
                        title="Handtaschen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2409-Handtaschen"
                      >
                        Handtaschen
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/4262.html"
                        title="Koffer-Sets"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/4262-Koffer-Sets"
                      >
                        Koffer-Sets
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/4212.html"
                        title="Damenuhren"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/4212-Damenuhren"
                      >
                        Damenuhren
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105546.html"
                        title="Herren-Pantoletten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105546-Herren-Pantoletten"
                      >
                        Herren-Pantoletten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/111350.html"
                        title="Herren-Unterhosen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/111350-Herren-Unterhosen"
                      >
                        Herren-Unterhosen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104659.html"
                        title="Herren-Bademäntel"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104659-Herren-Bademäntel"
                      >
                        Herren-Bademäntel
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/4224.html"
                        title="Ketten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/4224-Ketten"
                      >
                        Ketten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104575.html"
                        title="Herren-Badeschuhe"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104575-Herren-Badeschuhe"
                      >
                        Herren-Badeschuhe
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2412.html"
                        title="Rucksäcke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2412-Rucksäcke"
                      >
                        Rucksäcke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104658.html"
                        title="Bademäntel"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104658-Bademäntel"
                      >
                        Bademäntel
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/102949.html"
                        title="Damen-Flip-Flops"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/102949-Damen-Flip-Flops"
                      >
                        Damen-Flip-Flops
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104573.html"
                        title="Herren-Hausschuhe"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104573-Herren-Hausschuhe"
                      >
                        Herren-Hausschuhe
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104574.html"
                        title="Herren-Flip-Flops"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104574-Herren-Flip-Flops"
                      >
                        Herren-Flip-Flops
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2411.html"
                        title="Regenschirme"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2411-Regenschirme"
                      >
                        Regenschirme
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104592.html"
                        title="Trachtenmode"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104592-Trachtenmode"
                      >
                        Trachtenmode
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105362.html"
                        title="Kinder-Gummistiefel"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105362-Kinder-Gummistiefel"
                      >
                        Kinder-Gummistiefel
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105455.html"
                        title="Herren-Shorts"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105455-Herren-Shorts"
                      >
                        Herren-Shorts
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105018.html"
                        title="Herren-Trachten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105018-Herren-Trachten"
                      >
                        Herren-Trachten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103259.html"
                        title="Kinder-Regenbekleidung"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103259-Kinder-Regenbekleidung"
                      >
                        Kinder-Regenbekleidung
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100110.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100110-Mode &amp; Schuhe"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100110-Mode &amp; Schuhe">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <g>
    <path class="bde-icon-assortment-cls-1" d="M19,5H17a2,2,0,0,0-2,2v3H9V7A2,2,0,0,0,7,5H5A2,2,0,0,0,3,7v8a2,2,0,0,0,2,2H7a2,2,0,0,0,2-2V12h6v3a2,2,0,0,0,2,2h2a2,2,0,0,0,2-2V7A2,2,0,0,0,19,5ZM5,15V7H7v8Zm12,0V7h2v8Z"/>
    <rect class="bde-icon-assortment-cls-1" x="22" y="9" width="2" height="4"/>
    <rect class="bde-icon-assortment-cls-1" y="9" width="2" height="4"/>
  </g>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100061.html"
                    title="Sport &amp; Outdoor"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100061-Sport &amp; Outdoor"
                  >
                    Sport &amp; Outdoor
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106777.html"
                        title="E-Bikes"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106777-E-Bikes"
                      >
                        E-Bikes
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/105016.html"
                        title="Herren-Laufschuhe"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/105016-Herren-Laufschuhe"
                      >
                        Herren-Laufschuhe
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3700.html"
                        title="Wanderschuhe"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3700-Wanderschuhe"
                      >
                        Wanderschuhe
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3752.html"
                        title="Trampoline"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3752-Trampoline"
                      >
                        Trampoline
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103579.html"
                        title="Fahrradanhänger"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103579-Fahrradanhänger"
                      >
                        Fahrradanhänger
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103594.html"
                        title="Mountainbikes"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103594-Mountainbikes"
                      >
                        Mountainbikes
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103377.html"
                        title="Zelte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103377-Zelte"
                      >
                        Zelte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11321.html"
                        title="Kinderfahrräder"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11321-Kinderfahrräder"
                      >
                        Kinderfahrräder
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11518.html"
                        title="Trekkingrucksäcke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11518-Trekkingrucksäcke"
                      >
                        Trekkingrucksäcke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3740.html"
                        title="Laufbänder"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3740-Laufbänder"
                      >
                        Laufbänder
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3747.html"
                        title="Hanteln &amp; Gewichte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3747-Hanteln &amp; Gewichte"
                      >
                        Hanteln &amp; Gewichte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11702.html"
                        title="Fußballtore"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11702-Fußballtore"
                      >
                        Fußballtore
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11709.html"
                        title="Tischtennisplatten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11709-Tischtennisplatten"
                      >
                        Tischtennisplatten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3727.html"
                        title="Inline Skates"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3727-Inline Skates"
                      >
                        Inline Skates
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/113040.html"
                        title="Hallenschuhe"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/113040-Hallenschuhe"
                      >
                        Hallenschuhe
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3742.html"
                        title="Rudergeräte"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3742-Rudergeräte"
                      >
                        Rudergeräte
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103687.html"
                        title="Rollschuhe"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103687-Rollschuhe"
                      >
                        Rollschuhe
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3744.html"
                        title="Pulsuhren"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3744-Pulsuhren"
                      >
                        Pulsuhren
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/113323.html"
                        title="Self Balancing Scooter"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/113323-Self Balancing Scooter"
                      >
                        Self Balancing Scooter
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11723.html"
                        title="Snowboard-Boots"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11723-Snowboard-Boots"
                      >
                        Snowboard-Boots
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103886.html"
                        title="Tipp-Kick"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103886-Tipp-Kick"
                      >
                        Tipp-Kick
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107401.html"
                        title="Strandmuscheln"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107401-Strandmuscheln"
                      >
                        Strandmuscheln
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100061.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100061-Sport &amp; Outdoor"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100061-Sport &amp; Outdoor">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path class="bde-icon-assortment-cls-1" d="M22,3H2A2,2,0,0,0,0,5V18a2,2,0,0,0,2,2h9v2H7v2H17V22H13V20h9a2,2,0,0,0,2-2V5A2,2,0,0,0,22,3Zm0,2V15H2V5ZM2,18V17H22v1Z"/>
</svg>

            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100000.html"
                    title="Unterhaltungselektronik"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100000-Unterhaltungselektronik"
                  >
                    Unterhaltungselektronik
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2060.html"
                        title="Fernseher"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2060-Fernseher"
                      >
                        Fernseher
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/8937.html"
                        title="Kopfhörer"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/8937-Kopfhörer"
                      >
                        Kopfhörer
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10518.html"
                        title="PC Spiele"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10518-PC Spiele"
                      >
                        PC Spiele
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106732.html"
                        title="Tragbare Lautsprecher"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106732-Tragbare Lautsprecher"
                      >
                        Tragbare Lautsprecher
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/14083.html"
                        title="Konsolen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/14083-Konsolen"
                      >
                        Konsolen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/110068.html"
                        title="PS4 Spiele"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/110068-PS4 Spiele"
                      >
                        PS4 Spiele
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106886.html"
                        title="Digitale Sat-Receiver"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106886-Digitale Sat-Receiver"
                      >
                        Digitale Sat-Receiver
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/114183.html"
                        title="Nintendo Switch Spiele"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/114183-Nintendo Switch Spiele"
                      >
                        Nintendo Switch Spiele
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/8929.html"
                        title="CD-Radios"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/8929-CD-Radios"
                      >
                        CD-Radios
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/110144.html"
                        title="Xbox One Spiele"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/110144-Xbox One Spiele"
                      >
                        Xbox One Spiele
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2062.html"
                        title="Beamer"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2062-Beamer"
                      >
                        Beamer
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104672.html"
                        title="Blu-ray Player"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104672-Blu-ray Player"
                      >
                        Blu-ray Player
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106885.html"
                        title="DVB-C Receiver"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106885-DVB-C Receiver"
                      >
                        DVB-C Receiver
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2011.html"
                        title="MP3-Player"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2011-MP3-Player"
                      >
                        MP3-Player
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2013.html"
                        title="Plattenspieler"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2013-Plattenspieler"
                      >
                        Plattenspieler
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/29369.html"
                        title="DVD-Recorder"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/29369-DVD-Recorder"
                      >
                        DVD-Recorder
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/9663.html"
                        title="DVB-T2 Receiver"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/9663-DVB-T2 Receiver"
                      >
                        DVB-T2 Receiver
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106618.html"
                        title="FM-Transmitter"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106618-FM-Transmitter"
                      >
                        FM-Transmitter
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/10508.html"
                        title="Tragbare DVD-Player"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/10508-Tragbare DVD-Player"
                      >
                        Tragbare DVD-Player
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/107178.html"
                        title="Sat-Finder"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/107178-Sat-Finder"
                      >
                        Sat-Finder
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2053.html"
                        title="Tragbare CD-Player"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2053-Tragbare CD-Player"
                      >
                        Tragbare CD-Player
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/2032.html"
                        title="DVD-Player"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/2032-DVD-Player"
                      >
                        DVD-Player
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100000.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100000-Unterhaltungselektronik"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100000-Unterhaltungselektronik">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div><div class="xs-12-12 md-6-12 lg-4-12 xxlg-3-12 section">
          <div class="wrapper">
            <div class="svg-icon-assortment blue icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path class="bde-icon-assortment-cls-1" d="M21,8.55A5,5,0,0,0,16,4H8A5,5,0,0,0,3.05,8.55,3.49,3.49,0,0,0,0,12v5a2,2,0,0,0,2,2v1H4V19H20v1h2V19a2,2,0,0,0,2-2V12A3.49,3.49,0,0,0,21,8.55Zm-2,.31A3.49,3.49,0,0,0,17,12H13V6h3A3,3,0,0,1,19,8.86ZM8,6h3v6H7A3.49,3.49,0,0,0,5,8.86,3,3,0,0,1,8,6ZM22,17H2V12a1.5,1.5,0,0,1,3,0v1a1,1,0,0,0,1,1H18a1,1,0,0,0,1-1V12a1.5,1.5,0,0,1,3,0Z"/>
</svg>
            </div>
            <div class="section-links">
              <div class="title"><a
                    href="{{url('/assets')}}/show/kategorie/100176.html"
                    title="Wohnen &amp; Lifestyle"
                    class="econda-marker"
                    data-econda-marker="Events/Home_v6/All_Departments/100176-Wohnen &amp; Lifestyle"
                  >
                    Wohnen &amp; Lifestyle
                  </a></div><div class="flex-row sub-links"><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3474.html"
                        title="Küchenzeilen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3474-Küchenzeilen"
                      >
                        Küchenzeilen
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104605.html"
                        title="Betten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104605-Betten"
                      >
                        Betten
                      </a></div><div class="xs-12-12 link-column">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3482.html"
                        title="Kleiderschränke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3482-Kleiderschränke"
                      >
                        Kleiderschränke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3491.html"
                        title="Ecksofas"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3491-Ecksofas"
                      >
                        Ecksofas
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/11422.html"
                        title="Schlafsofas"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/11422-Schlafsofas"
                      >
                        Schlafsofas
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3492.html"
                        title="Sessel"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3492-Sessel"
                      >
                        Sessel
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103473.html"
                        title="Stühle"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103473-Stühle"
                      >
                        Stühle
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3924.html"
                        title="Kinderzimmer-Sets"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3924-Kinderzimmer-Sets"
                      >
                        Kinderzimmer-Sets
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3456.html"
                        title="Kommoden"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3456-Kommoden"
                      >
                        Kommoden
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3489.html"
                        title="Couchtische"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3489-Couchtische"
                      >
                        Couchtische
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3461.html"
                        title="Essgruppen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3461-Essgruppen"
                      >
                        Essgruppen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3457.html"
                        title="Schuhschränke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3457-Schuhschränke"
                      >
                        Schuhschränke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3423.html"
                        title="Deckenleuchten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3423-Deckenleuchten"
                      >
                        Deckenleuchten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104600.html"
                        title="Eckbänke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104600-Eckbänke"
                      >
                        Eckbänke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3495.html"
                        title="Wohnzimmerschränke"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3495-Wohnzimmerschränke"
                      >
                        Wohnzimmerschränke
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3483.html"
                        title="Komplette Schlafzimmer"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3483-Komplette Schlafzimmer"
                      >
                        Komplette Schlafzimmer
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103144.html"
                        title="Paravents"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103144-Paravents"
                      >
                        Paravents
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/3473.html"
                        title="Küchenwagen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/3473-Küchenwagen"
                      >
                        Küchenwagen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/106815.html"
                        title="Dekokamine"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/106815-Dekokamine"
                      >
                        Dekokamine
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104604.html"
                        title="Gästebetten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104604-Gästebetten"
                      >
                        Gästebetten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/103056.html"
                        title="Pantry-Küchen"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/103056-Pantry-Küchen"
                      >
                        Pantry-Küchen
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/104603.html"
                        title="Aufblasbare Betten"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/104603-Aufblasbare Betten"
                      >
                        Aufblasbare Betten
                      </a></div><div class="xs-12-12 link-column masked">
                      <a
                        class="link econda-marker"
                        href="{{url('/assets')}}/show/kategorie/100176.html"
                        title="Alle Unterkategorien"
                        data-econda-marker="Events/Home_v6/All_Departments/show_all/100176-Wohnen &amp; Lifestyle"
                      >
                        Alle Unterkategorien
                      </a><span class="css-icon-arrow small blue normal right all-categories"></span></div></div><div class="btn-collapsible" data-econda-marker="Events/Home_v6/All_Departments/show_more/100176-Wohnen &amp; Lifestyle">
                  <span>Weitere anzeigen</span>
                  <span class="css-icon-arrow small blue normal right all-categories"></span>
                </div>
                <div class="svg-icon-close gray btn-close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.41 17.41">
  <polygon class="bde-icon-close-cls-1" points="17.41 1.41 16 0 8.71 7.29 1.41 0 0 1.41 7.29 8.71 0 16 1.41 17.41 8.71 10.12 16 17.41 17.41 16 10.12 8.71 17.41 1.41"/>
</svg>
</div></div>
          </div>
        </div></div></div>
  <div id="popular-items" data-puzzle-compoid="popular_items">      <div class="component-heading">Die meist gesehenen Produkte</div>
        <div class="content-row">
      <div class="swipe-arrow left">
        <i class="css-icon-arrow large gray bold left"></i>
      </div>
      <div class="scroll-tiles scrollbar-horizontal-transparent scrollbar-hidden">
        <div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/32384-samsung-nu7179.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/32384-Samsung NU7179">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/zexMKHvuLGtNLg47QGZYJjmnDeOD-N_Ysmcqo-5Mi-SckuR/samsung-nu7179.jpg"
        width="200"
        height="200"
        alt="Samsung NU7179"
      ><div class="eu-labels">  <div class="eu-label eff-3"
          data-product-id="1315176277" >
    <span class="qualifier">A
    </span>
    <span class="triangle"></span>
  </div>
</div></div>

  <span class="name">Samsung NU7179</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>399,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">76 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/30625-samsung-galaxy-s9.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/30625-Samsung Galaxy S9">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/5JzAAvu-E8UJet8hi3cxg3mnDeOD-N_Ysmcqo-5Mi-SckuR/samsung-galaxy-s9.jpg"
        width="200"
        height="200"
        alt="Samsung Galaxy S9"
      ></div>

  <span class="name">Samsung Galaxy S9</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>469,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">276 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/26914-samsung-galaxy-s8.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/26914-Samsung Galaxy S8">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/fPAT0XXj0GGGvkY3cUScCbmnDeOD-N_Ysmcqo-5Mi-SckuR/samsung-galaxy-s8.jpg"
        width="200"
        height="200"
        alt="Samsung Galaxy S8"
      ></div>

  <span class="name">Samsung Galaxy S8</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>385,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">231 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/34695-dyson-cyclone-v10.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/34695-Dyson Cyclone V10">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/wt1a0BpyRc2lDKGeen3Wt_mnDeOD-N_Ysmcqo-5Mi-SckuR/dyson-cyclone-v10.jpg"
        width="200"
        height="200"
        alt="Dyson Cyclone V10"
      ></div>

  <span class="name">Dyson Cyclone V10</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>449,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">43 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/32851-hp-302.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/32851-HP 302">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/_949fbDMfrNxDRqCl4NvNHmnDeOD-N_Ysmcqo-5Mi-SckuR/hp-302.jpg"
        width="200"
        height="200"
        alt="HP 302"
      ></div>

  <span class="name">HP 302</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>10,78
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">921 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/27157-apple-iphone-8.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/27157-Apple iPhone 8">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/Apo-ZQIfGxZPqjmYXw-f07mnDeOD-N_Ysmcqo-5Mi-SckuR/apple-iphone-8.jpg"
        width="200"
        height="200"
        alt="Apple iPhone 8"
      ></div>

  <span class="name">Apple iPhone 8</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>479,97
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">288 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/31042-huawei-p20-lite.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/31042-Huawei P20 lite">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/Y2yTyrf1r2AvcJTXHvHBgLmnDeOD-N_Ysmcqo-5Mi-SckuR/huawei-p20-lite.jpg"
        width="200"
        height="200"
        alt="Huawei P20 lite"
      ></div>

  <span class="name">Huawei P20 lite</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>191,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">178 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/46926-samsung-galaxy-a7-2018.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/46926-Samsung Galaxy A7 (2018)">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/XCKJneCB-gQKKvw3HMGj9TmnDeOD-N_Ysmcqo-5Mi-SckuR/samsung-galaxy-a7-2018.jpg"
        width="200"
        height="200"
        alt="Samsung Galaxy A7 (2018)"
      ></div>

  <span class="name">Samsung Galaxy A7 (2018)</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>220,99
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">129 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/47727-apple-iphone-xr.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/47727-Apple iPhone XR">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/lPJSNSPLL8xqJsH2GdyzZXmnDeOD-N_Ysmcqo-5Mi-SckuR/apple-iphone-xr.jpg"
        width="200"
        height="200"
        alt="Apple iPhone XR"
      ></div>

  <span class="name">Apple iPhone XR</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>749,99
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">630 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/32856-hp-950.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/32856-HP 950">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/PlMlX10fJ2lOWLnXGOOzdrmnDeOD-N_Ysmcqo-5Mi-SckuR/hp-950.jpg"
        width="200"
        height="200"
        alt="HP 950"
      ></div>

  <span class="name">HP 950</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>19,98
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">396 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/32850-hp-301.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/32850-HP 301">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/GOZR5MBx5qZNJtkJFo2FrLmnDeOD-N_Ysmcqo-5Mi-SckuR/hp-301.jpg"
        width="200"
        height="200"
        alt="HP 301"
      ></div>

  <span class="name">HP 301</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>10,94
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">1568 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/33907-sodastream-crystal-2-0.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/33907-Sodastream Crystal 2.0">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/d7_Z7g6V1GkuiG3EtxL1JXmnDeOD-N_Ysmcqo-5Mi-SckuR/sodastream-crystal-2-0.jpg"
        width="200"
        height="200"
        alt="Sodastream Crystal 2.0"
      ></div>

  <span class="name">Sodastream Crystal 2.0</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>84,99
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">62 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/26804-nintendo-switch.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/26804-Nintendo Switch">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/SjKLTdupau2xKkWqt8IuhvmnDeOD-N_Ysmcqo-5Mi-SckuR/nintendo-switch.jpg"
        width="200"
        height="200"
        alt="Nintendo Switch"
      ></div>

  <span class="name">Nintendo Switch</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>299,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">130 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/32405-lg-c8.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/32405-LG C8">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/dUoX-YarL3nB7LN3A8jct_mnDeOD-N_Ysmcqo-5Mi-SckuR/lg-c8.jpg"
        width="200"
        height="200"
        alt="LG C8"
      ><div class="eu-labels">  <div class="eu-label eff-3"
          data-product-id="1318785134" >
    <span class="qualifier">A
    </span>
    <span class="triangle"></span>
  </div>
</div></div>

  <span class="name">LG C8</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>1.249,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">25 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/27158-apple-iphone-x.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/27158-Apple iPhone X">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/OjkLtQ02zvG7268ORgvdm7mnDeOD-N_Ysmcqo-5Mi-SckuR/apple-iphone-x.jpg"
        width="200"
        height="200"
        alt="Apple iPhone X"
      ></div>

  <span class="name">Apple iPhone X</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>838,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">132 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/21112-apple-iphone-6s.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/21112-Apple iPhone 6s">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/aVqMHzV8sVt2Ek5LwXBAuzmnDeOD-N_Ysmcqo-5Mi-SckuR/apple-iphone-6s.jpg"
        width="200"
        height="200"
        alt="Apple iPhone 6s"
      ></div>

  <span class="name">Apple iPhone 6s</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>298,99
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">238 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/46686-apple-watch-series-4.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/46686-Apple Watch Series 4">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/6oPsE2LWVjfY5zhLpSQCxXmnDeOD-N_Ysmcqo-5Mi-SckuR/apple-watch-series-4.jpg"
        width="200"
        height="200"
        alt="Apple Watch Series 4"
      ></div>

  <span class="name">Apple Watch Series 4</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>399,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">683 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/45692-apple-iphone-xs.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/45692-Apple iPhone XS">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/V_7NaCG5s_qWiulxI6p8lnmnDeOD-N_Ysmcqo-5Mi-SckuR/apple-iphone-xs.jpg"
        width="200"
        height="200"
        alt="Apple iPhone XS"
      ></div>

  <span class="name">Apple iPhone XS</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>980,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">331 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/25342-apple-iphone-7.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/25342-Apple iPhone 7">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/6fM7A2dfmYZZcTUsY4kFHDmnDeOD-N_Ysmcqo-5Mi-SckuR/apple-iphone-7.jpg"
        width="200"
        height="200"
        alt="Apple iPhone 7"
      ></div>

  <span class="name">Apple iPhone 7</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>279,97
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">413 Angebote im Preisvergleich</span>
    </a></div><div class="scroll-item tile">
    <a href="{{url('/assets')}}/baseproducts/31199-apple-ipad-9-7-2018.html"
       class="econda-marker headline"
       data-econda-marker="Events/Home_v6/Most_Seen_Products/31199-Apple iPad 9.7 (2018)">
        <div class="product-image"><img
        class="image"
        src="{{url('/assets')}}/cdn.billiger.com/dynimg/yH43LI7twx1kpeafBDR_ODmnDeOD-N_Ysmcqo-5Mi-SckuR/apple-ipad-9-7-2018.jpg"
        width="200"
        height="200"
        alt="Apple iPad 9.7 (2018)"
      ></div>

  <span class="name">Apple iPad 9.7 (2018)</span>
                                        <span class="price"><span class="price-show-prefix">ab</span>299,00
      <span>&euro;</span><sup class="price-star">*</sup>
    </span>
      

  <span class="offer-count">546 Angebote im Preisvergleich</span>
    </a></div>
      </div>
      <div class="swipe-arrow right">
        <i class="css-icon-arrow large gray bold right"></i>
      </div>
    </div></div>
  <div class="home-selected-items bottom-margin" data-puzzle-compoid="home_selected_items">
  <div class="component-heading">Für Sie zusammengestellt</div>
  <div class="component-frame flex-row"><div class="accordion-section xs-12-12 lg-6-12" id="toptopic">
        <div class="accordion-headline">
          <span>Top Themen</span>
          <ul class="slice-tabs">
                        <li>20</li><li>40</li></ul>
        </div>
        <div class="accordion-content slice-content">
          <ul class="flex-row"><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/2911-hd-karte.html" title="HD+ Karte">HD+ Karte</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/17342-chanel-no-5.html" title="Chanel No.5">Chanel No.5</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/12600-dolce-gusto-kapseln.html" title="Dolce Gusto Kapseln">Dolce Gusto Kapseln</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/6279-almased.html" title="Almased">Almased</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/12601-nutella.html" title="Nutella">Nutella</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/13725-aperol.html" title="Aperol">Aperol</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/15390-boxspringbett-140x200-cm.html" title="Boxspringbett 140x200 cm">Boxspringbett 140x200 cm</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/10536-boxspringbett.html" title="Boxspringbett">Boxspringbett</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/14034-gefrierschrank-no-frost.html" title="Gefrierschrank No Frost">Gefrierschrank No Frost</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/530-fernseher-mit-integriertem-receiver.html" title="Fernseher mit integriertem Receiver">Fernseher mit integriertem Receiver</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/9996-de-longhi-magnifica.html" title="De Longhi Magnifica">De Longhi Magnifica</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/14553-chanel-coco-mademoiselle.html" title="Chanel Coco Mademoiselle">Chanel Coco Mademoiselle</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/13111-braun-series-9.html" title="Braun Series 9">Braun Series 9</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/4117-senseo-pads.html" title="Senseo Pads">Senseo Pads</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/1656-rindenmulch.html" title="Rindenmulch">Rindenmulch</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/10230-genius-nicer-dicer.html" title="Genius Nicer Dicer">Genius Nicer Dicer</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/14388-philips-senseo.html" title="Philips Senseo">Philips Senseo</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/2224-ariel-waschmittel.html" title="Ariel Waschmittel">Ariel Waschmittel</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/8125-holzbrikett.html" title="Holzbrikett">Holzbrikett</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/9229-kuehlschrank-mit-gefrierfach.html" title="Kühlschrank mit Gefrierfach">Kühlschrank mit Gefrierfach</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/3830-nebeneingangstuer.html" title="Nebeneingangstür">Nebeneingangstür</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/593-avm-fritzbox.html" title="AVM Fritz!Box">AVM Fritz!Box</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/1933-spuelmaschine.html" title="Spülmaschine">Spülmaschine</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/15393-boxspringbett-180x200-cm.html" title="Boxspringbett 180x200 cm">Boxspringbett 180x200 cm</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/10077-stressless-sessel.html" title="Stressless Sessel">Stressless Sessel</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/12061-jaegermeister.html" title="Jägermeister">Jägermeister</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/1342-rattan-sonneninsel.html" title="Rattan-Sonneninsel">Rattan-Sonneninsel</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/38-boxspringbett-mit-bettkasten.html" title="Boxspringbett mit Bettkasten">Boxspringbett mit Bettkasten</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/13276-retro-kuehlschrank.html" title="Retro Kühlschrank">Retro Kühlschrank</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/6286-sessel-mit-aufstehhilfe.html" title="Sessel mit Aufstehhilfe">Sessel mit Aufstehhilfe</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/13794-imac.html" title="iMac">iMac</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/11761-wobenzym.html" title="Wobenzym">Wobenzym</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/3304-boxspringbett-160x200-cm.html" title="Boxspringbett 160x200 cm">Boxspringbett 160x200 cm</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/1067-schlaraffia-geltex.html" title="Schlaraffia Geltex">Schlaraffia Geltex</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/12554-einbaukuehlschrank-mit-gefrierfach.html" title="Einbaukühlschrank mit Gefrierfach">Einbaukühlschrank mit Gefrierfach</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/1340-fahrradtraeger-fuer-anhaengerkupplung.html" title="Fahrradträger für Anhängerkupplung">Fahrradträger für Anhängerkupplung</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/9921-adidas-zx-750.html" title="Adidas ZX 750">Adidas ZX 750</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/14500-chanel-chance.html" title="Chanel Chance">Chanel Chance</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/842-wpc-terrassendielen.html" title="WPC Terrassendielen">WPC Terrassendielen</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/topics/7355-metall-gartenhaus.html" title="Metall-Gartenhaus">Metall-Gartenhaus</a>
              </li></ul>
          <span class="show-less">Weitere anzeigen</span>
        </div>
      </div><div class="accordion-section xs-12-12 lg-6-12" id="topfilterindex">
        <div class="accordion-headline">
          <span>Top Filter</span>
          <ul class="slice-tabs">
                        <li>20</li><li>40</li></ul>
        </div>
        <div class="accordion-content slice-content">
          <ul class="flex-row"><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/4373/53176-apple-handys-ohne-vertrag.html" title="Apple Handys ohne Vertrag">Apple Handys ohne Vertrag</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/4373/52411-samsung-handys-ohne-vertrag.html" title="Samsung Handys ohne Vertrag">Samsung Handys ohne Vertrag</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/4373/53203-huawei-handys-ohne-vertrag.html" title="Huawei Handys ohne Vertrag">Huawei Handys ohne Vertrag</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/106860/26728-sodastream-wassersprudler.html" title="SodaStream Wassersprudler">SodaStream Wassersprudler</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/103526/80538-kuehlschrank-ohne-gefrierfach.html" title="Kühlschrank ohne Gefrierfach">Kühlschrank ohne Gefrierfach</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/3273/52746-weber-grills.html" title="WEBER Grills">WEBER Grills</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/9056/26761-kitchenaid-kuechenmaschinen.html" title="KitchenAid Küchenmaschinen">KitchenAid Küchenmaschinen</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/3551/46294-jura-kaffee-vollautomaten.html" title="Jura Kaffee-Vollautomaten">Jura Kaffee-Vollautomaten</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/107341/74476-sodastream-sirup.html" title="SodaStream Sirup">SodaStream Sirup</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/4373/70812-smartphone-unter-100-euro.html" title="Smartphone unter 100 €">Smartphone unter 100 €</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/3548/69042-nespresso-maschinen.html" title="Nespresso-Maschinen">Nespresso-Maschinen</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/105643/51097-tassimo-kaffeepads.html" title="Tassimo Kaffeepads">Tassimo Kaffeepads</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/107286/73962-jack-daniel-s-whisky.html" title="Jack Daniel&#39;s Whisky">Jack Daniel&#39;s Whisky</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/3596/85863-geschirrspueler-45-cm.html" title="Geschirrspüler 45 cm">Geschirrspüler 45 cm</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/10624/38981-moet-und-chandon-champagner.html" title="Moet &amp; Chandon Champagner">Moet &amp; Chandon Champagner</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/103234/29676-hp-tintenpatronen.html" title="HP Tintenpatronen">HP Tintenpatronen</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/3605/70808-waschmaschine-bis-200-euro.html" title="Waschmaschine bis 200 Euro">Waschmaschine bis 200 Euro</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/105546/28958-birkenstock-herren-pantoletten.html" title="BIRKENSTOCK Herren-Pantoletten">BIRKENSTOCK Herren-Pantoletten</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/4212/73975-michael-kors-damenuhren.html" title="Michael Kors Damenuhren">Michael Kors Damenuhren</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/103331/79169-pavillon-mit-festem-dach.html" title="Pavillon mit festem Dach">Pavillon mit festem Dach</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/2060/86048-smart-tv.html" title="Smart TV">Smart TV</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/100119/74559-giorgio-armani-uhren.html" title="Giorgio Armani Uhren">Giorgio Armani Uhren</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/3582/26744-brita-wasserfilter.html" title="Brita Wasserfilter">Brita Wasserfilter</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/104865/27905-makita-akkuschrauber.html" title="Makita Akkuschrauber">Makita Akkuschrauber</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/3605/86328-miele-waschmaschine.html" title="Miele Waschmaschine">Miele Waschmaschine</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/4373/52410-nokia-handys-ohne-vertrag.html" title="Nokia Handys ohne Vertrag">Nokia Handys ohne Vertrag</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/102965/53499-ugg-damenstiefel.html" title="UGG Damenstiefel">UGG Damenstiefel</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/3549/55553-pad-kaffeemaschine.html" title="Pad-Kaffeemaschine">Pad-Kaffeemaschine</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/3273/69914-gasgrill.html" title="Gasgrill">Gasgrill</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/114565/86350-ps4-controller.html" title="PS4 Controller">PS4 Controller</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/10628/38883-coca-cola-limonaden.html" title="Coca Cola Limonaden">Coca Cola Limonaden</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/103525/67487-kuehl-gefrierkombination-no-frost.html" title="Kühl-Gefrierkombination No-Frost">Kühl-Gefrierkombination No-Frost</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/8842/74986-tablet-mit-tastatur.html" title="Tablet mit Tastatur">Tablet mit Tastatur</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/3597/42895-kaercher-hochdruckreiniger.html" title="Kärcher Hochdruckreiniger">Kärcher Hochdruckreiniger</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/107288/38988-havana-club-rum.html" title="Havana Club Rum">Havana Club Rum</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/2416/50199-ray-ban-sonnenbrillen.html" title="Ray Ban Sonnenbrillen">Ray Ban Sonnenbrillen</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/103061/27148-ghd-glaetteisen.html" title="GHD Glätteisen">GHD Glätteisen</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/105359/29002-heelys-kinder-sneaker.html" title="HEELYS Kinder-Sneaker">HEELYS Kinder-Sneaker</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/104584/53550-ugg-kinder-stiefel.html" title="UGG Kinder-Stiefel">UGG Kinder-Stiefel</a>
              </li><li class="xs-12-12 lg-6-12">
                <a class="link" href="{{url('/assets')}}/categories/103930/57348-backofen-mit-mikrowelle.html" title="Backofen mit Mikrowelle">Backofen mit Mikrowelle</a>
              </li></ul>
          <span class="show-less">Weitere anzeigen</span>
        </div>
      </div></div>
</div>
  <div class="home-footer-text component-frame bottom-margin" data-puzzle-compoid="home_footer_text">
  <div class="inframe-heading">billiger.de - Ihr Preisvergleich</div>
  <div class="text-body">
    <p>
      billiger.de ist einer der bekanntesten und meistgenutzten Preisvergleiche Deutschlands (Quelle: 46. WWW-Benutzer-Analyse W3B, April/Mai 2018)&nbsp;und mit mehr als 50 Mio. Preisen zu über 1 Mio. Produkten auch einer der inhaltsstärksten. Dabei wurde billiger.de seit 2006 als einziger deutscher Preisvergleich regelmäßig mit einem TÜV-Zertifikat ausgezeichnet.
    </p>
    <p>
      Das Produktspektrum der über 22.500 Shops auf billiger.de reicht von Handys, PCs, Tablets, TV-Geräten und weiterer Unterhaltungselektronik über Arzneimittel, Haushalts- und Gartenbedarf bis hin zu Mode, Sport-, Spiel- und Freizeitartikeln. Daneben bietet billiger.de einen Preisvergleich für Strom- und Gastarife.
    </p>
    <p>
      Diesen täglich mehrfach aktualisierten Preisvergleich ergänzt billiger.de anhand eines Portfolios weiterführender relevanter Produktinformationen, darunter Produktvideos, Testberichte und Nutzerbewertungen. Damit unterstützt billiger.de den Internetnutzer nicht nur durch einen übersichtlichen Preisvergleich, sondern daneben mit einer umfassenden Beratung.
    </p>
  </div>
</div>
  <div
  style="display: none"
  data-puzzle-compoid="eu_label_layer">
</div>
@endsection